package main

import (
	mathRand "math/rand"
	"time"
)

func (b *board) update(y, x, v int) {
	// Updates the b at array Y and at index X of Y with v

	b.Board[y][x] = v
}

func (b *board) addGarbage() bool {
	// Check that nothing is in top row as would cause top out
	if !b.rowIsEmpty(0) {
		return false
	}

	// Shift everythign on the field up 1 row, starting at rowIndex 1
	for rowIndex := 1; rowIndex < BOARDHEIGHT; rowIndex++ {
		for x := 0; x < len(b.Board[rowIndex]); x++ {
			b.update(rowIndex-1, x, b.Board[rowIndex][x]) // setting value on the new row
			b.update(rowIndex, x, 0)                      // deleting copied value
		}
	}

	// Create a random garbage row and add it to the bottom row of the field
	garbageRow := [10]int{0, 1, 1, 1, 1, 1, 1, 1, 1, 1}
	mathRand.Seed(time.Now().UnixNano())
	mathRand.Shuffle(len(garbageRow), func(i, j int) { garbageRow[i], garbageRow[j] = garbageRow[j], garbageRow[i] })

	b.Board[BOARDHEIGHT-1] = garbageRow

	return true
}

func (b *board) clearRows() int {
	// clears a fully connected horizontal line on the board and moves everything down one row
	rowsClearedCount := 0
	rowsClearedIndices := make([]int, 0)
	for rowIndex := range b.Board {

		if b.clearFullRow(rowIndex) {
			rowsClearedCount++
			rowsClearedIndices = append(rowsClearedIndices, rowIndex)
		}

	}
	// move rows down aka add gravity
	b.moveRowsDown(rowsClearedIndices)

	return rowsClearedCount
}

func (b *board) moveRowsDown(rowsClearedIndices []int) {

	// for the rowIndex that is cleared and now empty, we want to move all the rows above it down
	for i := 0; i < len(rowsClearedIndices); i++ { //iterate through emptyrows

		for rowIndex := range b.Board {
			activeRowIndex := len(b.Board) - rowIndex // we need to reverse loop the arrays so we go from bottom to top

			// if the activeRowIndex is above (less than) the empty row index, move it down
			if activeRowIndex < rowsClearedIndices[i] {
				// make sure we dont move down an empty row
				if !b.rowIsEmpty(activeRowIndex) {
					b.moveRowDown(activeRowIndex)
				}
			}
		}
	}
}

func (b *board) moveRowDown(rowIndex int) {
	// copies the current row values to the row below ( above in the index)
	// and sets the values from which it copied to 0

	for x := 0; x < len(b.Board[rowIndex]); x++ {
		b.update(rowIndex+1, x, b.Board[rowIndex][x]) // setting value on the new row
		b.update(rowIndex, x, 0)                      // deleting copied value
	}
}

func (b *board) rowIsEmpty(rowIndex int) bool {
	// returns true if row is empty

	//catch out of range index
	// TODO: adjust the dependend code on this function so we can remove the below if
	if rowIndex >= BOARDHEIGHT {
		return false
	}

	for x := 0; x < len(b.Board[rowIndex]); x++ {
		if b.Board[rowIndex][x] != 0 {
			// row is not empty
			return false
		}
	}
	return true
}

func (b *board) clearFullRow(rowIndex int) bool {
	// clears a row if fully connected aka is full
	// does nothing if row is not fully connected
	// returns false if no row to be cleared is found true otherwise

	for x := 0; x < len(b.Board[rowIndex]); x++ {
		if b.Board[rowIndex][x] == 0 {
			// row is not full
			return false
		}
	}

	for x := 0; x < len(b.Board[rowIndex]); x++ {
		b.Board[rowIndex][x] = 0
	}

	return true
}

func (b *board) maxHeight() int {
	// Returns heights height occupied

	// TODO: we are calling b.heights() multiple times for different things (maxHeight and bumpiness)
	heights := b.heights()
	maxHeight := heights[0] // assume first value is the largest

	for _, height := range heights {
		if height > maxHeight {
			maxHeight = height // found another higher value, replace previous value in max
		}
	}
	return maxHeight

}

func (b *board) rowGap(rowIndex int) [][]int {
	// Returns the indeces of gaps in given rowIndex
	rowGap := make([][]int, 0)
	for x := 0; x < BOARDWIDTH; x++ {
		if b.Board[rowIndex][x] == 0 {
			rowGap = append(rowGap, []int{rowIndex, x})
		}
	}
	return rowGap
}

func (b *board) rowGaps() [][]int {
	// Returns indeces of gaps in rows with <= 2 gaps
	rowGaps := make([][]int, 0)
	for rowIndex := BOARDHEIGHT - 1; rowIndex >= 0; rowIndex-- { //iterate rows of board from bottom
		rowGap := b.rowGap(rowIndex) //get all gaps in a row
		if len(rowGap) <= 2 {        // if less than or equal to 2 gaps in the row
			rowGaps = append(rowGaps, rowGap...)
		}
	}
	return rowGaps
}
