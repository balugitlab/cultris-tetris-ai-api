package main

import (
	"fmt"
	"testing"
)

func TestSameTiles(t *testing.T) {

	boardStruct := &board{}

	boardStruct2 := &board{}

	tp := generatePiece("i")
	tp.spawnPieceOnBoard(boardStruct)
	tp.moveLeft(boardStruct)
	tp.drop(boardStruct)

	tp2 := generatePiece("i")
	tp2.spawnPieceOnBoard(boardStruct2)
	tp2.rotatePiece(boardStruct2, true)
	tp2.rotatePiece(boardStruct2, true)

	tp2.wallKickLeft(boardStruct2)
	tp2.moveRight(boardStruct2)
	tp2.moveRight(boardStruct2)
	tp2.drop(boardStruct2)

	tpAllTiles := append(tp.TilePositions, tp.CenterTilePosition)
	tp2AllTiles := append(tp2.TilePositions, tp2.CenterTilePosition)

	if !sameTiles(tpAllTiles, tp2AllTiles) {

		t.Error("Expected value!sameTiles(tpAllTiles, tp2AllTiles) to be true")

		boardStruct.showString()
		fmt.Println("-----------------------")
		boardStruct2.showString()
		fmt.Println("tp", tpAllTiles)
		fmt.Println("tp2", tp2AllTiles)

	}
}
