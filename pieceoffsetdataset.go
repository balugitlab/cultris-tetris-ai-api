package main

// package main

// Stores Offset Dataset for the pieces

// the first value represents the x offset aka BOARDWIDTH offset
// the second the y value aka the height
// {0, -1} == x=0, y=-1

// srs true rotation offset data
// var JLSTZ_Offsets = [4][5][2]int{
// 	{{0, 0}, {0, 0}, {0, 0}, {0, 0}, {0, 0}},
// 	{{1, 0}, {1, 0}, {1, -1}, {0, 2}, {1, 2}},
// 	{{0, 0}, {0, 0}, {0, 0}, {0, 0}, {0, 0}},
// 	{{0, 0}, {-1, 0}, {0, 0}, {0, 0}, {0, 0}}}
//
// var I_Offsets = [4][5][2]int{
// 	{{0, 0}, {-1, 0}, {2, 0}, {-1, 0}, {2, 0}},
// 	{{-1, 0}, {0, 0}, {0, 0}, {0, 1}, {0, -2}},
// 	{{-1, 1}, {1, 1}, {-2, 1}, {1, 0}, {-2, 0}},
// 	{{0, 1}, {0, 1}, {0, 1}, {0, -1}, {0, 2}}}
//
// var O_Offsets = [4][1][2]int{
// 	{{0, 0}},
// 	{{0, -1}},
// 	{{-1, -1}},
// 	{{-1, 0}}}

// C2Offsets ultris2 offset data
var C2Offsets = [8][2]int{
	{0, 0}, {-1, 0}, {+1, 0}, {0, +1}, {-1, +1}, {+1, +1}, {-2, 0}, {+2, 0},
}

var C2OffsetIPiece = [2][2]int{
	{0, 0}, {-1, 0},
}
