package main

func resolveCombo(c combo, completeLines int) combo {
	rowsClearedCount := float64(completeLines)

	// there was a line clear and the comboTime is not negative, so we increment the comboCounter
	if rowsClearedCount > 0 && c.comboTime >= 0 {
		c.comboCount++
	}

	// comboTime is not negative
	if c.comboTime >= 0 {

		// Punish for not clearing
		if rowsClearedCount == 0 {
			c.comboTime -= 0.42
			if c.comboCount > 1 {
				if c.comboCount < 5 {
					// the punsihment is increased when the comboCoun is above 5
					c.comboTime -= (c.comboCount - 1) * .015
				}
				c.comboTime -= (c.comboCount - 1) * .03

			}
		} else {
			switch c.comboCount {
			case 1:
				c.comboTime += 2.4 + rowsClearedCount*1.2
			case 2:
				c.comboTime += 1.1 + rowsClearedCount*0.6
			case 3:
				c.comboTime += 0.4 + rowsClearedCount*0.3
			case 4:
				c.comboTime += 0.05 + rowsClearedCount*0.15
			case 5:
				c.comboTime += 0.0 + rowsClearedCount*0.075
			case 6:
				c.comboTime += 0.0 + rowsClearedCount*0.0375
			case 7:
				c.comboTime += 0.0 + rowsClearedCount*0.01875 - 0.2
			case 8:
				c.comboTime += 0.0 + rowsClearedCount*0.009375 - 0.3
			case 9:
				c.comboTime += 0.0 + rowsClearedCount*0.0046875 - 0.4
			case 10:
				c.comboTime += 0.0 + rowsClearedCount*0 - 0.6
			case 11:
				c.comboTime += 0.0 + rowsClearedCount*0 - 0.8
			case 12:
				c.comboTime += 0.0 + rowsClearedCount*0 - 1
			}
		}
	}
	return c
}

func calculateLinesSent(comboSize int) int {
	var sent int
	switch comboSize {
	case 0:
		sent = 0
	case 1:
		sent = 0
	case 2:
		sent = 1
	case 3:
		sent = 2
	case 4:
		sent = 3
	case 5:
		sent = 5
	case 6:
		sent = 7
	case 7:
		sent = 10
	case 8:
		sent = 15
	case 9:
		sent = 22
	case 10:
		sent = 31
	case 11:
		sent = 42
	case 12:
		sent = 53
	case 13:
		sent = 64
	case 14:
		sent = 75
	case 15:
		sent = 86
	}
	return sent
}
