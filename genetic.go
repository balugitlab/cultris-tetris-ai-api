package main

import (
	"fmt"
	"math/rand"
	"time"
)

func fitnessSurvivor() int {

	// initialize stuff
	b := &board{}
	p := generateRandomPiece()
	nextP := generateRandomPiece()
	rowsCleared := 0

	for i := 1; i < 10000; i++ {

		// we do this before spawning the next piece on the board
		if i%6 == 0 {
			for k := 0; k < 1; k++ {
				if !b.addGarbage() {
					// returns false only if something is in top row aka topped out
					break
				}
			}
		}

		if !p.spawnPieceOnBoard(b) {
			// if false, topped out
			break
		}

		_, navigations, _ := b.findBestHardorSoftDropMove(&p, &nextP)

		// fmt.Println(i, "Current", p.Type, "Next", nextP.Type)
		// b.showString()
		// fmt.Println(score)
		// getInput()

		navigations.resolve(b, &p)
		// fmt.Println(navigations.mockresolve())

		rowsCleared = rowsCleared + b.clearRows()

		p = nextP
		nextP = generateRandomPiece()

	}
	return rowsCleared

}

func fitnessUberLeet() int {

	// initialize stuff
	b := &board{}
	p := generateRandomPiece()
	nextP := generateRandomPiece()
	rowsCleared := 0

	// comboLog holds an array where each element is a combo expressed as individCombo
	var comboLog []int

	for i := 1; i < 10000; i++ {

		// every 21 pieces we add 4 lines of garbage
		// at 200 bpm, 21 pieces are placed every 6.5 seconds
		// we do this before spawning the next piece on the board
		if i%21 == 0 {
			for k := 0; k < 4; k++ {
				if !b.addGarbage() {
					// returns false only if something is in top row aka topped out
					break
				}
			}
		}

		if !p.spawnPieceOnBoard(b) {
			// if false, topped out
			break
		}

		_, _, bestPO := b.findBestHardorSoftDropMove(&p, &nextP)

		// fmt.Println(i, "Current", p.Type, "Next", nextP.Type)
		// b.showString()
		// fmt.Println(score)
		// getInput()

		bestPO.Piece.NavigationRecord.resolve(b, &p)
		b.evaluate()
		// fmt.Printf("b board post eval "+"%+v", b.Heuristics)
		// fmt.Println(bestPO.Piece.NavigationRecord.mockresolve())

		// at 200 bpm, about 0.3 seconds is sepnt per move
		b.Heuristics.Combo.comboTime -= 0.3

		// if a combo ends, append
		if b.Heuristics.Combo.comboTime < 0 {
			b.Heuristics.Combo.comboTime = 0 //reset the timer
			if b.Heuristics.Combo.comboCount > 0 {
				comboLog = append(comboLog, int(b.Heuristics.Combo.comboCount))
			} else {
				comboLog = append(comboLog, 0)
			}
			b.Heuristics.Combo.comboCount = 0
		}

		rowsCleared = rowsCleared + b.clearRows()

		p = nextP
		nextP = generateRandomPiece()
	}
	var linesSent int
	for i := 0; i < len(comboLog); i++ {
		linesSent += calculateLinesSent(comboLog[i])
	}
	fmt.Println(comboLog, "Sent", linesSent)
	return linesSent
}

func fitness7bag() int {

	maxpieces := 10000
	numBags := maxpieces / 7
	pieceSeq := []string{}
	for i := 0; i < numBags; i++ {
		pieceSeq = append(pieceSeq, generate7bag()...)
	}

	b := &board{}
	rowsCleared := 0

	for i := 0; i < len(pieceSeq)-1; i++ {
		p := generatePiece(pieceSeq[i])
		nextP := generatePiece(pieceSeq[i+1])
		// fmt.Println("CUR AND NEXT PIECE:", p.Type, nextP.Type)

		if !p.spawnPieceOnBoard(b) {
			break
		}
		// fmt.Println("-----------------------")
		// b.show()
		// fmt.Println("-----------------------")
		// fmt.Println("bord after SpawnPieceONboard")

		_, navigations := b.findBestHardDropMove(&p, &nextP)

		navigations.resolve(b, &p)

		// fmt.Println("-----------------------")
		// b.show()
		// fmt.Println("-----------------------")
		// fmt.Println("bord after resolve...Score is", b.evaluate())

		rowsCleared = rowsCleared + b.clearRows()
		// fmt.Println(rowsCleared)

	}

	return rowsCleared
}

func fitnessRandomPieces() int {

	// initialize stuff
	b := &board{}

	p := generateRandomPiece()
	nextP := generateRandomPiece()
	rowsCleared := 0

	for i := 0; i < 10000; i++ {
		if !p.spawnPieceOnBoard(b) {
			// b.show()
			break
		}

		_, navigations := b.findBestHardDropMove(&p, &nextP)

		navigations.resolve(b, &p)

		rowsCleared = rowsCleared + b.clearRows()

		p = nextP
		nextP = generateRandomPiece()
	}
	return rowsCleared
}

func fitnessGivenPiece() int {

	b := &board{}
	rowsCleared := 0
	piece := "i"

	p := generatePiece(piece)
	nextP := generatePiece(piece)

	for i := 0; i < 1000; i++ {
		if !p.spawnPieceOnBoard(b) {
			b.show()
			break
		}

		_, navigations := b.findBestHardDropMove(&p, &nextP)

		navigations.resolve(b, &p)

		rowsCleared = rowsCleared + b.clearRows()

		p = nextP
		nextP = generatePiece(piece)
	}
	return rowsCleared
}

func findWeightsRandom() {
	rand.Seed(time.Now().UnixNano())

	results := make(chan int, 100)

	for routines := 0; routines < 100; routines++ {
		go func() {
			for index := 0; index < 1000000; index++ {

				maxHeightWeight = randFloat(-1, 0)
				completeLinesWeight = randFloat(0, 1)
				holesWeight = randFloat(-1, 0)
				bumpinessWeight = randFloat(-1, 0)

				linesCleared := fitnessRandomPieces()
				if linesCleared > 100 {
					fmt.Println(linesCleared)
					fmt.Println(maxHeightWeight, completeLinesWeight, holesWeight, bumpinessWeight)
					results <- linesCleared
				}

			}
		}()
	}
	<-results
}

func random(min int, max int) int {
	return rand.Intn(max-min) + min
}

func randFloat(min, max float64) float64 {
	return min + rand.Float64()*(max-min)
}
