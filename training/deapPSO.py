from deap import base
from deap import creator
from deap import tools
from scoop import futures
import numpy as np
import time
import operator
import random
import pickle
import requests
import json
import aiohttp
import asyncio


# deap.creator - initialize create particle function
creator.create("FitnessMax", base.Fitness, weights=(1.0,))
creator.create(
    "Particle",
    list,
    fitness=creator.FitnessMax,
    speed=list,
    smin=None,
    smax=None,
    best=None,
)

# creates a particle of size (number of weights or heuristics), minimum weight pmin, maximum weight pmax
# smin, smax are speed limits for particle velocity during updateParticle
def generate(size, pmin, pmax, smin, smax):
    part = creator.Particle(random.uniform(pmin, pmax) for _ in range(size))
    part.speed = [random.uniform(smin, smax) for _ in range(size)]
    part.smin = smin
    part.smax = smax
    return part


# updates a particle by computing speed, limiting speed, moving particle to a new position
def updateParticle(part, best, phi1, phi2):
    u1 = (random.uniform(0, phi1) for _ in range(len(part)))
    u2 = (random.uniform(0, phi2) for _ in range(len(part)))
    v_u1 = map(operator.mul, u1, map(operator.sub, part.best, part))
    v_u2 = map(operator.mul, u2, map(operator.sub, best, part))
    part.speed = list(map(operator.add, part.speed, map(operator.add, v_u1, v_u2)))
    for i, speed in enumerate(part.speed):
        if speed < part.smin:
            part.speed[i] = part.smin
        elif speed > part.smax:
            part.speed[i] = part.smax
    part[:] = list(map(operator.add, part, part.speed))


# Evaluation function: takes a particle, passes it to a fitness function which returns a score
def evalOneMax(individual):
    scores = []
    for i in range(game_attempts):

        # scores.append(Tetris.run_game(n=individual, render=False))
        # replace with interface to go here, with structure of passing individual (particle)
        # scores.append(np.random.randn())
        keys = [
            "aggregatedHeightWeight",
            "completeLinesWeight",
            "holesWeight",
            "bumpinessWeight",
            "blocksAboveHole1Weight",
            "blocksAboveHole2Weight",
            "rowTransAboveHole1Weight",
        ]
        part_dict = dict(zip(keys, individual))
        url = "http://localhost:9876/evaluate/{}/{}/{}/{}/{}/{}/{}".format(
            part_dict["aggregatedHeightWeight"],
            part_dict["completeLinesWeight"],
            part_dict["holesWeight"],
            part_dict["bumpinessWeight"],
            part_dict["blocksAboveHole1Weight"],
            part_dict["blocksAboveHole2Weight"],
            part_dict["rowTransAboveHole1Weight"]
        )

        r = requests.get(url)
        score = float(json.loads(r.text))
        scores.append(score)

    # print("Particle had fitness of", scores, "Mean:", int(np.mean(scores)))

    return (np.average(scores),)


def evalOneMaxAsync(individual):
    async def eval(session, url):
        async with session.get(url) as resp:
            return await resp.text()

    async def test(individual):
        keys = [
            "aggregatedHeightWeight",
            "completeLinesWeight",
            "holesWeight",
            "bumpinessWeight",
            "blocksAboveHole1Weight",
            "blocksAboveHole2Weight",
            "rowTransAboveHole1Weight",
        ]
        part_dict = dict(zip(keys, individual))
        url = "http://localhost:9876/evaluate/{}/{}/{}/{}/{}/{}/{}".format(
            part_dict["aggregatedHeightWeight"],
            part_dict["completeLinesWeight"],
            part_dict["holesWeight"],
            part_dict["bumpinessWeight"],
            part_dict["blocksAboveHole1Weight"],
            part_dict["blocksAboveHole2Weight"],
            part_dict["rowTransAboveHole1Weight"]
        )

        async with aiohttp.ClientSession() as session:
            return await eval(session, url)

    scores = []
    for i in range(game_attempts):
        loop = asyncio.get_event_loop()
        score = loop.run_until_complete(test(individual))
        scores.append(float(score))

    print("Particle:", individual, "Fitness:", scores, "Mean:", int(np.mean(scores)))

    return (np.average(scores),)


# toolbox holds all the operators after registering them
toolbox = base.Toolbox()
# generate particles
toolbox.register("particle", generate, size=7, pmin=-3, pmax=3, smin=-0.5, smax=0.5)
# generate a population of particles using toolbox.population(n=popsize)
toolbox.register("population", tools.initRepeat, list, toolbox.particle)
# updates a particle
toolbox.register("update", updateParticle, phi1=2.0, phi2=2.0)
# map for multiprocessing with scoop futures
toolbox.register("map", futures.map)
# evaluate a single particle
toolbox.register("evaluate", evalOneMax)


def main(checkpoint):
    try:
        # A file name has been given, then load the data from the file
        with open(checkpoint, "rb") as cp_file:
            cp = pickle.load(cp_file, encoding="bytes")  # load checkpoint
            pop = cp["population"]  # load population from checkpoing
            g = cp["generation"]  # load generation number from checkpoing
            best = cp["best"]  # load best particle from checkpoing
            print("resuming from checkpoint at generation:", g)
    except:
        # Start a new evolution
        # random.seed(64)
        pop = toolbox.population(
            n=population_size
        )  # initialize population of particles
        g = 0
        print("Start of evolution")
        best = None

    # Some statistics using deap.tools.statistics
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", np.mean)
    stats.register("std", np.std)
    stats.register("min", np.min)
    stats.register("max", np.max)

    logbook = tools.Logbook()
    logbook.header = ["gen", "evals"] + stats.fields

    # iterate for NGEN generations
    while g < NGEN:
        g += 1
        start_time = time.time()
        print("-- Generation %i --" % g)

        individual_part = [part for part in pop]  # list of particles
        # map evaluation of particle in list particles and store in fitnessess
        fitnesses = toolbox.map(toolbox.evaluate, individual_part)
        # update particle fitness values from fitnesses
        for part, fit in zip(individual_part, fitnesses):
            part.fitness.values = fit

        # for each particle in population
        for part in pop:
            if not part.best or part.best.fitness < part.fitness:
                part.best = creator.Particle(part)
                part.best.fitness.values = part.fitness.values
            if not best or best.fitness < part.fitness:
                best = creator.Particle(part)
                best.fitness.values = part.fitness.values

        for part in pop:
            toolbox.update(part, best)

        # Gather all the fitnesses in one list and print the stats
        logbook.record(gen=g, evals=len(pop), **stats.compile(pop))
        log = logbook.stream

        print(log)
        print(
            "Generation",
            g,
            "took:",
            time.strftime("%H:%M:%S", time.gmtime(time.time() - start_time)),
        )
        print(best, "\n")

        # dump the best to a text file
        with open("PSOoutput.txt", "a") as text_file:
            text_file.writelines([log, "\n", str(best), "\n"])

        # every FREQ generations
        if g % FREQ == 0:
            # Fill the dictionary using the dict(key=value[, ...]) constructor
            cp = dict(population=pop, generation=g, best=best)
            # dump the population to a checkpoint file as a dictionary for saving progress
            with open("PSO_checkpoint.pkl", "wb") as cp_file:
                pickle.dump(cp, cp_file)

    return pop, logbook, best


NGEN = 500  # number of generations to iterate through
population_size = 150  # number of particles in population
game_attempts = 10  # number of game attempts for particle evaluation
FREQ = 1  # how often to save checkpoint file


if __name__ == "__main__":
    main(checkpoint="PSO_checkpoint.pkl")

    # --------------------------------------------------------------------------------------------
    # Requests works, but fails with multiprocessign map using python3 -m scoop deapPSO.py

    # part = toolbox.particle()
    # keys = ["aggregatedHeightWeight","completeLinesWeight","holesWeight","bumpinessWeight", "blocksAboveHole1Weight", "blocksAboveHole2Weight"]
    # part_dict = dict(zip(keys, part))
    # print(part_dict)
    # url = "http://localhost:9876/evaluate/{}/{}/{}/{}/{}/{}".format(part_dict["aggregatedHeightWeight"],part_dict["completeLinesWeight"],part_dict["holesWeight"],part_dict["bumpinessWeight"], part_dict["blocksAboveHole1Weight"], part_dict["blocksAboveHole2Weight"])
    #
    #
    # r0 = time.time()
    # r = requests.get(url)
    #
    # print("Requests took", time.time() - r0)
    # print(r.text)
    # score = float(json.loads(r.text))
    # print(score)

    # --------------------------------------------------------------------------------------------
    # aiohttp works, but fails with async and multiprocessign map
    # Just change line 111 toolbox.evaluate to toolbox.register("evaluate", evalOneMaxAsync)
    # and run python3 -m scoop deapPSO.py to test

    # async def eval(session, url):
    #         async with session.get(url) as resp:
    #             return await resp.text()
    #
    # async def test_main():
    #     part = toolbox.particle()
    #     keys = ["aggregatedHeightWeight","completeLinesWeight","holesWeight","bumpinessWeight"]
    #     part_dict = dict(zip(keys, part))
    #     # print(part_dict)
    #     url = "http://localhost:9876/evaluate/{}/{}/{}/{}".format(part_dict["aggregatedHeightWeight"],part_dict["completeLinesWeight"],part_dict["holesWeight"],part_dict["bumpinessWeight"])
    #     print('URL', url)
    #
    #     async with aiohttp.ClientSession() as session:
    #         score = await eval(session, url)
    #         print(score)
    #
    # loop = asyncio.get_event_loop()
    # loop.run_until_complete(test_main())
