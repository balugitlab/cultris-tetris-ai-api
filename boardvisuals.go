package main

import (
	"bytes"
	"fmt"
	"strconv"
)

func arrayToString(A [10]int, delim string) string {

	var buffer bytes.Buffer
	for i := 0; i < len(A); i++ {
		s := strconv.Itoa(A[i])
		if s == "0" {
			buffer.WriteString(" ")
		} else {
			buffer.WriteString(s)
		}
		if i != len(A)-1 {
			buffer.WriteString(delim)
		}
	}

	return buffer.String()
}

func (b *board) showString() {
	// shows the board in a formated way
	for i := range b.Board {
		fmt.Println(arrayToString(b.Board[i], " "))
	}
}

func (b *board) show() {
	// shows the board in a formated way
	for i := range b.Board {
		fmt.Println((b.Board[i]))
	}
}

func (b *board) showIndexed() {
	// shows the board in a formated way

	for i := range b.Board {
		fmt.Println("Index:", i, b.Board[i])
	}
}
