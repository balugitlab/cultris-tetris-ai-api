package main

// 1 per piece
// spawn tiles in proper location
// contains movement/rotation functions

//

func (p *piece) isFloating(b *board) bool {

	internalPiece := p.deepCopy()
	internalBoard := *b

	isFloating := true

	//bottom checks
	if internalPiece.CenterTilePosition.YPosition == BOARDHEIGHT-1 {
		return false
	}

	for i := range internalPiece.TilePositions {
		if internalPiece.TilePositions[i].YPosition == BOARDHEIGHT-1 {
			return false
		}
	}

	if internalBoard.Board[internalPiece.CenterTilePosition.YPosition+1][internalPiece.CenterTilePosition.XPosition] != 0 {
		return false
	}

	for i := range internalPiece.TilePositions {
		if internalBoard.Board[internalPiece.TilePositions[i].YPosition+1][internalPiece.TilePositions[i].XPosition] != 0 {
			return false
		}
	}

	return isFloating
}

func (p *piece) deepCopy() piece {

	// returns a deepCopy of p

	copyPiece := piece{}
	copyPiece.RawForm = p.RawForm // TODO: DeepCopy this aswell
	copyPiece.Type = p.Type
	copyPiece.TilePositions = make([]tile, len(p.TilePositions))
	copy(copyPiece.TilePositions, p.TilePositions)
	copyPiece.CenterTilePosition = p.CenterTilePosition
	copyPiece.RotationIndex = p.RotationIndex
	copyPiece.NavigationRecord = make(navigations, len(p.NavigationRecord))
	copy(copyPiece.NavigationRecord, p.NavigationRecord)

	// copier.Copy(copyPiece, p) this is NOT WORKING

	return copyPiece
}

func (p *piece) width() int {
	// return the width of p in its current rotation form

	if p.Type == "o" {
		return 2
	}

	// p is horizontal
	if p.RotationIndex == 0 || p.RotationIndex == 2 {

		switch p.Type {

		case "i":
			return 4

		case "l":
			return 3

		case "j":
			return 3

		case "s":
			return 4

		case "z":
			return 4

		case "t":
			return 3

		}
	}

	// p is vertical
	switch p.Type {

	case "i":
		return 1

	case "l":
		return 2

	case "j":
		return 2

	case "s":
		return 2

	case "z":
		return 2

	case "t":
		return 2

	}

	return 10
}

func (p *piece) remove(b *board) {

	// removes p from b
	// sets the b tiles that correspond to the p tiles to 0

	b.update(p.CenterTilePosition.YPosition, p.CenterTilePosition.XPosition, 0)

	for tileindex := range p.TilePositions {

		b.update(p.TilePositions[tileindex].YPosition, p.TilePositions[tileindex].XPosition, 0)
	}
}

func (p *piece) add(b *board) {

	// adds p to b
	// sets the tiles from p with value 1 or 2 on b

	b.update(p.CenterTilePosition.YPosition, p.CenterTilePosition.XPosition, 2)

	for tileindex := range p.TilePositions {

		b.update(p.TilePositions[tileindex].YPosition, p.TilePositions[tileindex].XPosition, 1)
	}
}

func (p *piece) drop(b *board) bool {
	// drops the piece to the bottom
	for p.moveDown(b) {
	}
	return true
}

func (p *piece) wallKickLeft(b *board) bool {
	// kicks p to the left most possible position
	for p.moveLeft(b) {
	}
	return true
}

func (p *piece) wallKickRight(b *board) bool {
	// kicks p to the right most possible position
	for p.moveRight(b) {
	}
	return true
}

func (p *piece) offset(b *board) bool {
	// fmt.Println(p.Type, p.RotationIndex)

	// offsets the piece with the offset data
	// this makes wall kicks and spins possible
	// offset can be a confusing name as it does not offset anything if not needed
	// it only actually offsets pieces when next to a wall or when doing spins, when the rotation is obstructed
	// returns false if it failed
	// which means that placemente is not possible and rotation should be reversed

	offsetC2Piece := func() bool {

		for _, offset := range C2Offsets {

			offsetX := offset[0]
			offsetY := offset[1]

			// apply offset

			// center tile
			p.CenterTilePosition.XPosition += offsetX
			p.CenterTilePosition.YPosition += offsetY

			// normale tiles
			for tileindex := range p.TilePositions {
				p.TilePositions[tileindex].XPosition += offsetX
				p.TilePositions[tileindex].YPosition += offsetY

			}

			// check if piece can be placed like this
			if !p.isOutOfBound() && !p.isColliding(b) {
				// piece is placable returning
				return true
			}

			// piece was not placable with offset data
			// reseting to original state
			p.CenterTilePosition.XPosition -= offsetX
			p.CenterTilePosition.YPosition -= offsetY

			// normale tiles
			for tileindex := range p.TilePositions {
				p.TilePositions[tileindex].XPosition -= offsetX
				p.TilePositions[tileindex].YPosition -= offsetY

			}
		}

		return false
	}

	offsetC2PieceI := func() bool {

		if p.RotationIndex == 3 {
			offsetX, offsetY := C2OffsetIPiece[0][0], C2OffsetIPiece[0][1]

			// apply offset

			// center tile
			p.CenterTilePosition.XPosition += offsetX
			p.CenterTilePosition.YPosition += offsetY

			// normale tiles
			for tileindex := range p.TilePositions {
				p.TilePositions[tileindex].XPosition += offsetX
				p.TilePositions[tileindex].YPosition += offsetY

			}

			// check if piece can be placed like this
			if !p.isOutOfBound() && !p.isColliding(b) {
				// piece is placable returning
				return true
			}

			// piece was not placable with offset data
			// reseting to original state
			p.CenterTilePosition.XPosition -= offsetX
			p.CenterTilePosition.YPosition -= offsetY

			// normale tiles
			for tileindex := range p.TilePositions {
				p.TilePositions[tileindex].XPosition -= offsetX
				p.TilePositions[tileindex].YPosition -= offsetY

			}

		}

		if p.RotationIndex == 1 {
			offsetX, offsetY := C2OffsetIPiece[1][0], C2OffsetIPiece[1][1]

			// apply offset

			// center tile
			p.CenterTilePosition.XPosition += offsetX
			p.CenterTilePosition.YPosition += offsetY

			// normale tiles
			for tileindex := range p.TilePositions {
				p.TilePositions[tileindex].XPosition += offsetX
				p.TilePositions[tileindex].YPosition += offsetY

			}

			// check if piece can be placed like this
			if !p.isOutOfBound() && !p.isColliding(b) {
				// piece is placable returning
				return true
			}

			// piece was not placable with offset data
			// reseting to original state
			p.CenterTilePosition.XPosition -= offsetX
			p.CenterTilePosition.YPosition -= offsetY

			// normale tiles
			for tileindex := range p.TilePositions {
				p.TilePositions[tileindex].XPosition -= offsetX
				p.TilePositions[tileindex].YPosition -= offsetY

			}

		}

		return offsetC2Piece()

	}

	if p.Type == "i" {
		return offsetC2PieceI()
	}
	return offsetC2Piece()

	// Partial or wrong code for srs implementation maybe it is usefull in the future

	// offsetO := func() {
	// 	activeOffset := O_Offsets[p.RotationIndex][0]
	// 	activeOffsetX := activeOffset[0]
	// 	activeOffsetY := activeOffset[1]
	//
	// 	// apply offset
	// 	// p.TilePositions = append(p.TilePositions, p.CenterTilePosition)
	//
	// 	for tileIndex := range p.TilePositions {
	//
	// 		fmt.Println("Positions Before Offset:", p.TilePositions[tileIndex].XPosition, p.TilePositions[tileIndex].YPosition)
	//
	// 		fmt.Println(activeOffsetX, activeOffsetY)
	// 		p.TilePositions[tileIndex].XPosition += activeOffsetX
	// 		p.TilePositions[tileIndex].YPosition += activeOffsetY
	//
	// 		fmt.Println("Positions After Offset:", p.TilePositions[tileIndex].XPosition, p.TilePositions[tileIndex].YPosition)
	//
	// 	}
	//
	// 	// remove the center tile
	// }
	// offsetO()

	// offsetI := func() {
	// 	// apply offset
	// 	activeOffset := I_Offsets[p.RotationIndex]
	//
	// 	fmt.Println(p.RotationIndex)
	//
	// 	for _, v := range activeOffset {
	// 		activeOffsetX := v[0]
	// 		activeOffsetY := v[1]
	//
	// 		fmt.Println("offset X:", activeOffsetX)
	// 		fmt.Println("offset Y:", activeOffsetY)
	//
	// 		// offsetting
	//
	// 		// center tile
	// 		p.CenterTilePosition.XPosition += activeOffsetX
	// 		p.CenterTilePosition.YPosition += activeOffsetY
	//
	// 		for index := range p.TilePositions {
	//
	// 			fmt.Println("Position before increment", p.TilePositions[index].XPosition, p.TilePositions[index].YPosition)
	// 			p.TilePositions[index].XPosition += activeOffsetX
	// 			p.TilePositions[index].YPosition += activeOffsetY
	//
	// 			fmt.Println("Position after increment", p.TilePositions[index].XPosition, p.TilePositions[index].YPosition)
	// 		}
	//
	// 		// check if this is a valid position
	// 		if !p.isOutOfBound() || !p.isColliding(b) {
	// 			fmt.Println("rotatet succesefully")
	// 			fmt.Println("CenterTile is", p.CenterTilePosition)
	// 			fmt.Println("Normale tiles are", p.TilePositions)
	// 			// piece can be placed braking out of loop
	// 			break
	// 		} else {
	// 			// reseting the piece to its original state
	// 			// center tile
	// 			p.CenterTilePosition.XPosition -= activeOffsetX
	// 			p.CenterTilePosition.YPosition -= activeOffsetY
	//
	// 			for index := range p.TilePositions {
	// 				p.TilePositions[index].XPosition -= activeOffsetX
	// 				p.TilePositions[index].YPosition -= activeOffsetY
	// 			}
	// 		}
	// 	}
	// }
}

func (p *piece) isOutOfBound() bool {
	// a wrapper function for tile.isOutOfBound

	// center tile check
	if p.CenterTilePosition.isOutOfBound() {
		return true
	}

	for _, v := range p.TilePositions {
		if v.isOutOfBound() {
			return true
		}
	}

	return false
}

func (p *piece) isColliding(b *board) bool {
	// a wrapper function for tile.isColliding

	// center tile check
	if p.CenterTilePosition.isColliding(b) {
		return true
	}

	for _, v := range p.TilePositions {
		if v.isColliding(b) {
			return true
		}
	}

	return false
}

func (p *piece) moveUp(b *board) {

	// NOTE: this function is kinda broken
	// TODO: update

	// make a copy of the positions to revert to if isOutofBound detected
	oldTilePositons := make([]tile, len(p.TilePositions))
	copy(oldTilePositons, p.TilePositions)

	isOutofBound := false

	// checking for collision
	// decrementing == moving up
	p.CenterTilePosition.decrementY()
	if p.CenterTilePosition.isOutOfBound() {
		isOutofBound = true
	}
	//normal tiles
	for i := range p.TilePositions {
		p.TilePositions[i].decrementY()
		if p.TilePositions[i].isOutOfBound() {
			isOutofBound = true
		}
	}

	if !isOutofBound {
		// Update board
		// delete old piece tiles
		// delete old tiles of piece from board -1 because we incremented/decremented them above
		b.update(p.CenterTilePosition.YPosition+1, p.CenterTilePosition.XPosition, 0)

		// delete normal tiles
		for i := range p.TilePositions {
			b.update(p.TilePositions[i].YPosition+1, p.TilePositions[i].XPosition, 0)
		}

		// Update board with new piece tiles
		// center tile
		b.update(p.CenterTilePosition.YPosition, p.CenterTilePosition.XPosition, 2)

		// normal tiles
		for i := range p.TilePositions {
			b.update(p.TilePositions[i].YPosition, p.TilePositions[i].XPosition, 1)
		}
	} else {
		p.TilePositions = oldTilePositons
		p.CenterTilePosition.incrementY()
	}
}

func (p *piece) rotatePiece(b *board, rotateClockwise bool) {

	// BUG: when rotating at the bottom of the board when the board is empty piece vanishes afaik has no other affect than the visual

	// check for O Piece
	// O pieces dont get rotated
	if p.Type == "o" {
		return
	}

	p.remove(b)

	// maybe there is a better way to do this rotation index stuff
	// maybe just use if statement and decrease and increas and when and catch if it goes above 4 or below 0
	if rotateClockwise {
		// we have to invert this because our setup is inverted
		p.RotationIndex-- // increment RotationIndex
	} else {
		p.RotationIndex++ // decrement RotationIndex
	}
	p.RotationIndex = rotationMod(p.RotationIndex, 4) // set the active rotation index e.g rotationMod(5,4) == rotation index 1

	// rotate the TilePositions of p
	for i := range p.TilePositions {
		p.TilePositions[i].rotateTile(p.CenterTilePosition, rotateClockwise)
	}

	// rotation offset aka placement check
	if !p.offset(b) {
		// 	rotated piece can not be placed on board
		// 	rotating it back and returning
		for i := range p.TilePositions {
			p.TilePositions[i].rotateTile(p.CenterTilePosition, !rotateClockwise)
		}
		return
	}

	// add navigation record
	rotationRecord := navigation{}
	if rotateClockwise {
		rotationRecord.RotateRight = true
	} else {
		rotationRecord.RotateLeft = true
	}
	p.NavigationRecord = append(p.NavigationRecord, rotationRecord)

	// Update the board
	// set the new tiles on the actual board
	p.add(b)
	return
}

func (p *piece) spawnPieceOnBoard(b *board) bool {

	// no checks are currently performed

	offset := p.getInsertionOffset()
	placeWasFree := true

	for index, rows := range p.RawForm {
		for i, rowItem := range rows {

			// spawn the piece in the middle of the board
			insertPositionX := offset + i // offset it with the index of the rowItem and the offset index of the piece
			insertPositionY := index + 2  // insertion height

			if b.Board[insertPositionY][insertPositionX] != 0 {
				// piece cant be spawned
				placeWasFree = false
			}

			b.Board[insertPositionY][insertPositionX] = rowItem // apply the piece aka insert the piece

			// set the tile positions for the piece
			p.initTilePosition(insertPositionX, insertPositionY, rowItem)

		}
	}
	return placeWasFree
}

func (p *piece) initTilePosition(positionX, positionY, tileType int) {

	// Initializes the CenterTilePosition or appends to the TilePositions of a piece
	// with the given X and Y position
	// which is an INTERNAL representation of where the piece is
	// this does not interact with the board!
	// NOT to be used to update the tile positions
	// Only call this function once per piece (4x per tile)

	// check for 0 values
	if tileType == 0 {
		return
	}

	// check for center tile which would have a value of 2 rather than 1
	if tileType == 2 {
		// tile is center tile
		p.CenterTilePosition.XPosition = positionX
		p.CenterTilePosition.YPosition = positionY
		return
	}

	// Normal tiles
	tile := new(tile)
	tile.XPosition = positionX
	tile.YPosition = positionY

	p.TilePositions = append(p.TilePositions, *tile)
	return
}

func (p *piece) getInsertionOffset() int {
	// returns the amount p should be offseted
	// when spawning/inserting it on the board
	// so the piece spawns in the middle

	// i guess we could hardcode 3
	// don't really see the problem of o being one tile of

	// just to be explicit all pieces are represented
	switch p.Type {
	case "o":
		return 4

	case "i":
		return 3

	case "l":
		return 3

	case "j":
		return 3

	case "s":
		return 3

	case "z":
		return 3

	case "t":
		return 3

	default:
		return 3
	}
}
