package main

// 1 per tile (4 per piece)
// contains the mathematical calculations for movemente
// and rotation functions

func (t *tile) isColliding(b *board) bool {
	// returns true if t cant be placed on b aka position is already occupied

	if b.Board[t.YPosition][t.XPosition] != 0 {
		return true
	}

	return false
}

func (t *tile) isOutOfBound() bool {
	// returns true if t is larger than the BOARDWIDTH or BOARDHEIGHT

	// hint the tile positions are starting at index 0
	// so if XPosition is == BOARDWIDTH we are out of bound
	if t.XPosition >= BOARDWIDTH || t.YPosition >= BOARDHEIGHT || t.XPosition < 0 || t.YPosition < 0 {
		return true
	}

	return false
}

func (t *tile) rotateTile(centerTilePosition tile, rotateClockwise bool) {

	relativeTile := t.getRelativeTilePosition(centerTilePosition)

	if !rotateClockwise {

		// get new relative X and Y values
		newRelativeX := (0 * relativeTile.XPosition) + (1 * relativeTile.YPosition)
		newRelativeY := (-1 * relativeTile.XPosition) + (0 * relativeTile.YPosition)

		newAbsoluteX := centerTilePosition.XPosition + newRelativeX
		newAbsoluteY := centerTilePosition.YPosition + newRelativeY

		//updating Tile position
		t.XPosition = newAbsoluteX
		t.YPosition = newAbsoluteY
		return
	}

	if rotateClockwise {

		// get new relative X and Y values
		newRelativeX := (0 * relativeTile.XPosition) + (-1 * relativeTile.YPosition)
		newRelativeY := (1 * relativeTile.XPosition) + (0 * relativeTile.YPosition)

		newAbsoluteX := centerTilePosition.XPosition + newRelativeX
		newAbsoluteY := centerTilePosition.YPosition + newRelativeY

		//updating Tile position
		t.XPosition = newAbsoluteX
		t.YPosition = newAbsoluteY
		return

	}

}

func (t *tile) getRelativeTilePosition(centerTilePosition tile) tile {

	relativeTile := tile{}
	relativeTile.XPosition = t.XPosition - centerTilePosition.XPosition
	relativeTile.YPosition = t.YPosition - centerTilePosition.YPosition

	return relativeTile
}

func (t *tile) incrementX() {
	t.XPosition++
}

func (t *tile) decrementX() {
	t.XPosition--
}

func (t *tile) incrementY() {
	t.YPosition++
}

func (t *tile) decrementY() {
	t.YPosition--
}
