package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"github.com/labstack/echo"
)

type bestMove struct {
	MoveList []string `json:"MoveList" xml:"MoveList"`
	Soft     bool     `json:"Soft" xml:"Soft"`
	Field    *board   `json:"Field" xml:"Field"`
	Score    float64  `json:"Score" xml:"Score"`
	Clears   int      `json:"Clears" xml:"Clears"`
}

func startAPI() {
	// API
	e := echo.New()
	// Routes
	e.GET("/", hello)
	e.GET("/evaluate/:maxHeightWeight/:completeLinesWeight/:holesWeight/:bumpinessWeight/:blocksAboveHole1Weight/:blocksAboveHole2Weight/:rowTransAboveHole1Weight/:comboCountWeight/:comboTimeIncrementWeight", evaluateAPI)
	e.GET("/bestmove/:field/:tetromino/:nexttetromino/:combotime/:combocounter", bestmoveAPI)
	// Server
	e.Logger.Fatal(e.Start(":9876"))
}

func hello(c echo.Context) error {
	return c.String(http.StatusOK, "Hello, World!")
}

func bestmoveAPI(c echo.Context) error {
	// Given a piece and board, evaluates and returns the best move
	var moveList []string
	field := c.Param("field")
	tetromino := c.Param("tetromino")
	nexttetromino := c.Param("nexttetromino")
	combotime, _ := strconv.ParseFloat(c.Param("combotime"), 64)
	combocounter, _ := strconv.ParseFloat(c.Param("combocounter"), 64)

	jsonstring, _ := url.QueryUnescape(field)
	var listoflists [][]string
	board := &board{}
	board.Heuristics.Combo.comboCount = combocounter
	board.Heuristics.Combo.comboTime = combotime
	p := generatePiece(tetromino)
	nextPiece := generatePiece(nexttetromino)

	json.NewDecoder(strings.NewReader(jsonstring)).Decode(&listoflists)
	// fmt.Println(err)
	for i, list := range listoflists {
		for j, value := range list {
			intVal, _ := strconv.Atoi(value)
			board.update(i, j, intVal)
		}
	}
	p.spawnPieceOnBoard(board)

	fmt.Println(p.Type, nextPiece.Type, combotime, combocounter)
	fmt.Println("Board given to API:")
	board.showString()

	score, _, bestPO := board.findBestHardorSoftDropMove(&p, &nextPiece)
	if bestPO.Soft == false {
		moveList = bestPO.Piece.NavigationRecord.smartresolve()
	} else {
		moveList = bestPO.Piece.NavigationRecord.smartresolveSoft()
	}

	// fmt.Println("-----------------------")
	bestPO.Piece.NavigationRecord.resolve(board, &p)
	board.evaluate()
	// fmt.Printf("b board post eval "+"%+v", board.Heuristics)
	clears := board.clearRows()
	fmt.Println("Solved board from API:")
	fmt.Println("move list", moveList)
	fmt.Println("")
	board.showString()
	u := &bestMove{
		MoveList: moveList,
		Soft:     bestPO.Soft,
		Field:    board,
		Score:    score,
		Clears:   clears,
	}

	return c.JSON(http.StatusOK, u)
}

func evaluateAPI(c echo.Context) error {
	// Evaluates a set of weights and returns a score
	maxHeightWeight, _ = strconv.ParseFloat(c.Param("maxHeightWeight"), 64)
	completeLinesWeight, _ = strconv.ParseFloat(c.Param("completeLinesWeight"), 64)
	holesWeight, _ = strconv.ParseFloat(c.Param("holesWeight"), 64)
	bumpinessWeight, _ = strconv.ParseFloat(c.Param("bumpinessWeight"), 64)
	blocksAboveHole1Weight, _ = strconv.ParseFloat(c.Param("blocksAboveHole1Weight"), 64)
	blocksAboveHole2Weight, _ = strconv.ParseFloat(c.Param("blocksAboveHole2Weight"), 64)
	rowTransAboveHole1Weight, _ = strconv.ParseFloat(c.Param("rowTransAboveHole1Weight"), 64)
	comboCountWeight, _ = strconv.ParseFloat(c.Param("comboCountWeight"), 64)
	comboTimeIncrementWeight, _ = strconv.ParseFloat(c.Param("comboTimeIncrementWeight"), 64)

	fmt.Println(maxHeightWeight, completeLinesWeight, holesWeight, bumpinessWeight, blocksAboveHole1Weight, blocksAboveHole2Weight, rowTransAboveHole1Weight, comboCountWeight, comboTimeIncrementWeight)

	score := strconv.Itoa(fitnessSurvivor())
	fmt.Println("Scored:", score)

	return c.String(http.StatusOK, score)
}
