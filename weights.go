package main

var (
	//default values
	// aggregatedHeightWeight   = -1.8945309716463648
	// completeLinesWeight      = 2.45950816384269
	// holesWeight              = -2.177404746429197
	// bumpinessWeight          = -0.41760960940410763
	// blocksAboveHole1Weight   = 0.0
	// blocksAboveHole2Weight   = 0.0
	// rowTransAboveHole1Weight = 0.0
	// comboCountWeight         = 0.0
	// comboTimeIncrementWeight = 0.0

	maxHeightWeight          = -0.648113840927302
	completeLinesWeight      = 3.7612208142114563
	holesWeight              = -5.03037327669136
	bumpinessWeight          = -0.9371775993665055
	blocksAboveHole1Weight   = -0.8492154644901556
	blocksAboveHole2Weight   = -0.5154815691619173
	rowTransAboveHole1Weight = -0.5825387507514859
	comboCountWeight         = 0.0
	comboTimeIncrementWeight = 0.0
	wellWeight               = 0.0
	wellIndexWeight          = [10]int{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0}
)

//[-0.648113840927302, 3.7612208142114563, -5.03037327669136, -0.9371775993665055, -0.8492154644901556, -0.5154815691619173, -0.5825387507514859]

//[-1.7710642861197154, 2.327028393275259, -5.676923245511798, -1.2335361681156674, -1.6544481232356603, -0.8252559573337852, -1.0010911565626492]
