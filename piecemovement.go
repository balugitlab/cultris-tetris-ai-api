package main

func (p *piece) navigationRecordOptimizer() {

	leftMove := navigation{}
	leftMove.MoveLeft = true

	rightMove := navigation{}
	rightMove.MoveRight = true

	optimizedNavigationRecord := navigations{}

	for i := 0; i < len(p.NavigationRecord); i++ {

		if i > len(p.NavigationRecord)-2 {
			break
		}

		if (p.NavigationRecord[i] == leftMove && p.NavigationRecord[i+1] == rightMove) || p.NavigationRecord[i] == rightMove && p.NavigationRecord[i+1] == leftMove {
			// we moved to one direction  and then immidiatly to the oposite again so we can remove this step

			// increment i to skip the next element on the next iteratin
			i += 2

			continue

		}

		// add the needed navigation to the optimizedNavigationRecord
		optimizedNavigationRecord = append(optimizedNavigationRecord, p.NavigationRecord[i])
	}

	p.NavigationRecord = optimizedNavigationRecord
}

// func move... will move the piece in the given direction if posible or will return false
// pure movement no locks are performed

func (p *piece) moveLeft(b *board) bool {

	// remove p from the board to avoid self colisions
	p.remove(b)

	// moving left
	p.decX()

	if p.isOutOfBound() || p.isColliding(b) {

		p.incrX() // moving it back
		p.add(b)  // adding it back
		return false

	}

	// add the movement to navigations
	m := navigation{}
	m.MoveLeft = true
	p.NavigationRecord = append(p.NavigationRecord, m)

	// Update board with new piece tiles
	p.add(b)

	return true
}

func (p *piece) moveRight(b *board) bool {

	// remove p from the board to avoid self colisions
	p.remove(b)

	// moving right
	p.incrX()

	if p.isOutOfBound() || p.isColliding(b) {

		p.decX() // moving it back
		p.add(b) // adding it back
		return false
	}

	// add the movement to navigations
	m := navigation{}
	m.MoveRight = true
	p.NavigationRecord = append(p.NavigationRecord, m)

	// Update board with new piece tiles
	p.add(b)

	return true
}

func (p *piece) moveDown(b *board) bool {

	// remove p from the board to avoid self colisions
	p.remove(b)

	// moving down
	p.incrY()

	// is p out of bound or colliding?
	if p.isOutOfBound() || p.isColliding(b) {

		p.decY() // moving it back up
		p.add(b) // add piece back to board

		return false
	}

	// add the movement to navigations
	m := navigation{}
	m.MoveDown = true
	p.NavigationRecord = append(p.NavigationRecord, m)

	// Update board with new piece tiles
	p.add(b)

	return true
}

func (p *piece) incrX() {
	// increments all the X values of p
	// which essentially moves it to the right on normal matrix

	// incrementing
	p.CenterTilePosition.incrementX()
	for i := range p.TilePositions {
		p.TilePositions[i].incrementX()
	}

}

func (p *piece) incrY() {
	// increments all the Y values of p
	// which essentially moves it down on normal matrix

	// incrementing
	p.CenterTilePosition.incrementY()
	for i := range p.TilePositions {
		p.TilePositions[i].incrementY()
	}

}

func (p *piece) decX() {
	// decrements all the X values of p
	// which essentially moves it to the left on normal matrix

	// decrementing
	p.CenterTilePosition.decrementX()
	for i := range p.TilePositions {
		p.TilePositions[i].decrementX()
	}
}

func (p *piece) decY() {

	// decrements all the Y values of p
	// which essentially moves it up on normal matrix

	// decrementing
	p.CenterTilePosition.decrementY()
	for i := range p.TilePositions {
		p.TilePositions[i].decrementY()
	}

}
