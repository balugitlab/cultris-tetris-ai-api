package main

import (
	"crypto/rand"
	"math/big"
	mathRand "math/rand"
	"time"
)

// 1 per game
// Spawns Pieces

func generatePiece(pieceType string) piece {
	piece := piece{}
	piece.Type = pieceType
	piece.RawForm = generateRawPiece(pieceType)
	return piece
}

func generateRandomPiece() piece {
	pieces := []string{"o", "j", "l", "t", "s", "z", "i"}

	nBig, err := rand.Int(rand.Reader, big.NewInt(7))
	if err != nil {
		panic(err)
	}
	n := nBig.Int64()

	piece := piece{}
	piece.Type = pieces[n]
	piece.RawForm = generateRawPiece(pieces[n])
	return piece
}

func generate7bag() []string {
	bag := []string{"o", "j", "l", "t", "s", "z", "i"}
	mathRand.Seed(time.Now().UnixNano())
	mathRand.Shuffle(len(bag), func(i, j int) { bag[i], bag[j] = bag[j], bag[i] })

	return bag
}

func generateRawPiece(pieceType string) [][]int {
	// returns the piece in a multidimensional array form

	piece := [][]int{}
	switch pieceType {

	case "o":
		piece = [][]int{
			{1, 1},
			{2, 1},
		}

	case "i":
		piece = [][]int{
			{1, 1, 2, 1},
		}

	case "l":
		piece = [][]int{
			{0, 0, 1},
			{1, 2, 1},
		}

	case "j":
		piece = [][]int{
			{1, 0, 0},
			{1, 2, 1},
		}

	case "s":
		piece = [][]int{
			{0, 1, 1},
			{1, 2, 0},
		}

	case "z":
		piece = [][]int{
			{1, 1, 0},
			{0, 2, 1},
		}

	case "t":
		piece = [][]int{
			{0, 1, 0},
			{1, 2, 1},
		}
	}
	return piece
}
