package main

// TILE
type tile struct {
	YPosition int
	XPosition int
}

// PIECE
type piece struct {
	RawForm            [][]int // Represents the raw form of the piece aka a two dimensional slice
	Type               string  // t,i,o etc...
	TilePositions      []tile
	CenterTilePosition tile
	RotationIndex      int
	NavigationRecord   navigations // holds the navigation instructions on how to reproduce the movement the piece had and to come to the same positions the piece is currently
}

// BOARD
const (
	// BOARDWIDTH width of board
	BOARDWIDTH = 10
	// BOARDHEIGHT height of board. Note that heuristics tests chnage with changing board height.
	BOARDHEIGHT = 20
)

type board struct {
	// represents a tetris board
	// Arrays do not need to be initialized explicitly;
	// the zero value of an array is a ready-to-use
	// the x and y zero point is at the top left corner

	// when refering to a Colum we speak about the a single line on the y achises aka BOARDHEIGHT
	// when refering to a row we speak about the second array of board.Board

	Board      [BOARDHEIGHT][BOARDWIDTH]int
	Heuristics heuristics
}

type heuristics struct {
	// contains properties of tetris board that we want to track from one board state to the next
	Combo              combo
	maxHeight          int
	holesCount         int
	blocksAboveHole1   int
	blocksAboveHole2   int
	completeLines      int
	bumpiness          int
	rowTransAboveHole1 int
	wellDepth          int
	wellIndex          int
}

type combo struct {
	comboTime  float64
	comboCount float64
}

// PLACEMENT OPTION
type placementOption struct {
	Board board
	Piece piece
	Score float64
	Soft  bool
}
