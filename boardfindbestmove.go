package main

import (
	"math"
)

func (b *board) generateAllPlacementOptionsCurrRotation(p *piece) []placementOption {

	validPlacements := []placementOption{}
	internalPiece := p.deepCopy()
	internalBoard := *b

	internalPiece.remove(&internalBoard)

	// move piece to bottom left
	// move piece to the left most position
	for {
		internalPiece.decX()
		if internalPiece.isOutOfBound() {
			internalPiece.incrX()
			break
		}
	}
	// move piece to the bottom most position
	for {
		internalPiece.incrY()
		if internalPiece.isOutOfBound() {
			internalPiece.decY()
			break
		}
	}

	// looping over the rest of the placements - we go left to right through a row, then go up a row and repeat
	for {
		for {

			// check if piece fits in current position
			if !internalPiece.isColliding(&internalBoard) && !internalPiece.isFloating(&internalBoard) {
				internalPiece.add(&internalBoard)
				// TODO: check if that placement is reachable and valid

				// Creating a option for this placement and appending to valid placement list
				option := placementOption{}
				option.Board = internalBoard
				option.Piece = internalPiece.deepCopy()
				option.Score = internalBoard.evaluate()
				validPlacements = append(validPlacements, option)

				// remove the piece from internalBoard
				internalPiece.remove(&internalBoard)
			}

			//iterate through row left to right
			internalPiece.incrX()
			if internalPiece.isOutOfBound() {
				internalPiece.decX()
				break
			}

		}
		// end of a row

		// iterate upwards
		internalPiece.decY()
		if internalPiece.isOutOfBound() {
			internalPiece.incrY()
			break
		}
		// move the piece all the way back to the left
		for {
			internalPiece.decX()
			if internalPiece.isOutOfBound() {
				internalPiece.incrX()
				break
			}
		}

	}
	return validPlacements
}

func (b *board) getIdealPlacement(p *piece) placementOption {

	validPlacements := []placementOption{}
	internalPiece := p.deepCopy()
	internalBoard := *b

	zeroRotationPlacementOptions := internalBoard.generateAllPlacementOptionsCurrRotation(&internalPiece)

	// rotate piece
	internalPiece.rotatePiece(&internalBoard, true)

	firstRotationPlacementOptions := internalBoard.generateAllPlacementOptionsCurrRotation(&internalPiece)

	// rotate piece
	// TODO: Only pieces T L J have 4 rotations. O has 1. S Z I have 2
	internalPiece.rotatePiece(&internalBoard, true)

	secondRotationPlacementOptions := internalBoard.generateAllPlacementOptionsCurrRotation(&internalPiece)

	// rotate piece
	internalPiece.rotatePiece(&internalBoard, true)

	thirdRotationPlacementOptions := internalBoard.generateAllPlacementOptionsCurrRotation(&internalPiece)

	// appending the options
	validPlacements = append(validPlacements, zeroRotationPlacementOptions...)
	validPlacements = append(validPlacements, firstRotationPlacementOptions...)
	validPlacements = append(validPlacements, secondRotationPlacementOptions...)
	validPlacements = append(validPlacements, thirdRotationPlacementOptions...)

	bestpOption := validPlacements[0]
	for i := 0; i < len(validPlacements); i++ {
		pOption := validPlacements[i]
		if pOption.Score > bestpOption.Score {
			bestpOption = pOption

		}
		// pOption.Board.show()
		// fmt.Println(pOption.Score)
		// fmt.Println("-----------------------")
	}
	// bestpOption.Board.show()
	// fmt.Println(bestpOption.Score)
	return bestpOption
}

func (b *board) findBestMove(p *piece) {

	p.remove(b)

	// Harddrop moves

	// spin moves

	// teleportation moves

	// slide moves

	p.add(b)
}

// findBestMove.... returns the evaluation score of the best move found for its section
// with instructions on how to get there

func (b *board) generateHardDropPlacementOptions(p *piece) []placementOption {

	internalPiece := p.deepCopy()
	internalBoard := *b
	placementOptions := []placementOption{}

	internalPieceZero := p.deepCopy()
	zeroRotationPlacementOptions := append(internalBoard.generateHardDropPlacementOptionsForCurRotationLeft(&internalPieceZero), internalBoard.generateHardDropPlacementOptionsForCurRotationRight(&internalPieceZero)...)
	placementOptions = append(placementOptions, zeroRotationPlacementOptions...)
	internalPieceZero.remove(&internalBoard)

	// Only pieces T L J have 4 rotations states (3 plus no rotation). O has 1 state. S Z I have 2 (1 rotation plus no rotation)
	if internalPiece.Type != "o" {
		// rotate piece CW
		internalPieceOne := p.deepCopy()
		internalPieceOne.rotatePiece(&internalBoard, true)

		firstRotationPlacementOptions := append(internalBoard.generateHardDropPlacementOptionsForCurRotationLeft(&internalPieceOne), internalBoard.generateHardDropPlacementOptionsForCurRotationRight(&internalPieceOne)...)
		placementOptions = append(placementOptions, firstRotationPlacementOptions...)
		internalPieceOne.remove(&internalBoard)

		if internalPiece.Type != "s" && internalPiece.Type != "z" && internalPiece.Type != "i" {
			internalPieceTwo := p.deepCopy()
			// rotate piece 180
			internalPieceTwo.rotatePiece(&internalBoard, true)
			internalPieceTwo.rotatePiece(&internalBoard, true)

			secondRotationPlacementOptions := append(internalBoard.generateHardDropPlacementOptionsForCurRotationLeft(&internalPieceTwo), internalBoard.generateHardDropPlacementOptionsForCurRotationRight(&internalPieceTwo)...)
			placementOptions = append(placementOptions, secondRotationPlacementOptions...)
			internalPieceTwo.remove(&internalBoard)

			// rotate piece CCW
			internalPieceThree := p.deepCopy()
			internalPieceThree.rotatePiece(&internalBoard, false)

			thirdRotationPlacementOptions := append(internalBoard.generateHardDropPlacementOptionsForCurRotationLeft(&internalPieceThree), internalBoard.generateHardDropPlacementOptionsForCurRotationRight(&internalPieceThree)...)
			placementOptions = append(placementOptions, thirdRotationPlacementOptions...)
			internalPieceThree.remove(&internalBoard)

		}
	}

	return placementOptions
}

func (b *board) generateHardDropPlacementOptionsForCurRotationLeft(p *piece) []placementOption {

	// internalPiece := p.deepCopy()
	internalBoard := *b
	placementOptions := []placementOption{}

	dropAndCreateOption := func(b board, p *piece) placementOption {

		internalP := p.deepCopy()
		internalB := b // this has no functionality other than making it more readable/clear
		option := placementOption{}

		internalP.drop(&internalB)
		option.Board = internalB
		option.Piece = internalP.deepCopy()
		option.Score = internalB.evaluate()
		return option

	}

	// internalPieceLefttMoves := p.deepCopy()
	// for internalPieceLefttMoves.moveLeft(&internalBoard) {
	//
	// 	placementOptions = append(placementOptions, dropAndCreateOption(internalBoard, &internalPieceLefttMoves))
	// }

	// internalPiece.wallKickLeft(&internalBoard)
	// placementOptions = append(placementOptions, dropAndCreateOption(internalBoard, &internalPiece))

	internalPieceLefttMoves := p.deepCopy()
	placementOptions = append(placementOptions, dropAndCreateOption(internalBoard, &internalPieceLefttMoves))
	for internalPieceLefttMoves.moveLeft(&internalBoard) {
		placementOptions = append(placementOptions, dropAndCreateOption(internalBoard, &internalPieceLefttMoves))
	}
	return placementOptions
}

func (b *board) generateHardDropPlacementOptionsForCurRotationRight(p *piece) []placementOption {

	// internalPiece := p.deepCopy()
	internalBoard := *b
	placementOptions := []placementOption{}

	dropAndCreateOption := func(b board, p *piece) placementOption {

		internalP := p.deepCopy()
		internalB := b // this has no functionality other than making it more readable/clear
		option := placementOption{}

		internalP.drop(&internalB)
		option.Board = internalB
		option.Piece = internalP.deepCopy()
		option.Score = internalB.evaluate()
		return option

	}

	// internalPieceLefttMoves := p.deepCopy()
	// for internalPieceLefttMoves.moveLeft(&internalBoard) {
	//
	// 	placementOptions = append(placementOptions, dropAndCreateOption(internalBoard, &internalPieceLefttMoves))
	// }

	// internalPiece.wallKickLeft(&internalBoard)
	// placementOptions = append(placementOptions, dropAndCreateOption(internalBoard, &internalPiece))

	internalPieceRightMoves := p.deepCopy()
	placementOptions = append(placementOptions, dropAndCreateOption(internalBoard, &internalPieceRightMoves))
	for internalPieceRightMoves.moveRight(&internalBoard) {
		placementOptions = append(placementOptions, dropAndCreateOption(internalBoard, &internalPieceRightMoves))
	}
	return placementOptions
}

func (b *board) findBestHardDropMove(p *piece, nextPiece *piece) (float64, navigations) {

	// returns the the score of the best move with the instructions on how to get there
	navigationInstuctions := navigations{}
	var bestScore float64

	internalP := p.deepCopy()
	internalNextP := nextPiece.deepCopy()
	internalB := *b

	// get the movement options for the current piece
	placementOptionsCurPiece := internalB.generateHardDropPlacementOptions(&internalP)
	bestScore = math.Inf(-8)
	for _, plo := range placementOptionsCurPiece {

		internalNextP.spawnPieceOnBoard(&plo.Board)
		placementOptionsNextPiece := plo.Board.generateHardDropPlacementOptions(&internalNextP)

		for _, nextplo := range placementOptionsNextPiece {
			// fmt.Println(i, "Current", plo.Piece.Type, "Next", nextplo.Piece.Type, nextplo.Score)
			// fmt.Println("currpiece tilepos:", plo.Piece.TilePositions)
			// fmt.Println("nextpiece tilepos:", nextplo.Piece.TilePositions)
			// nextplo.Board.show()
			// getInput()
			if nextplo.Score > bestScore {
				bestScore = nextplo.Score
				navigationInstuctions = plo.Piece.NavigationRecord
			}
		}
	}
	return bestScore, navigationInstuctions
}

// func (b *board) findBestSlideMove(p *piece) {
//
// 	internalPiece := p.deepCopy()
// 	internalBoard := *b
// }
//
// func (b *board) generateSlidePlacementOptions(p *piece) {
// 	internalPiece := p.deepCopy()
// 	internalBoard := *b
// 	placementOptions := []placementOption{}
//
// }

func (b *board) generateSlidePlacementOptionsForCurRotation(p *piece) []placementOption {

	internalPiece := p.deepCopy()
	internalBoard := *b
	placementOptions := []placementOption{}

	dropSlideAndCreateOptions := func(b board, p *piece) []placementOption {

		internalP := p.deepCopy()
		internalB := b // this has no functionality other than making it more readable/clear
		options := []placementOption{}

		slideLeftAndCreateOptions := func(b board, p *piece) []placementOption {

			internalP := p.deepCopy()
			internalB := b // this has no functionality other than making it more readable/clear
			options := []placementOption{}

			for internalP.moveLeft(&internalB) {
				// try to drop the piece again cuz maybe it goes down even further after we moved left
				// and i think we only want to be at the lowest point
				internalP.drop(&internalB)
				option := placementOption{}

				option.Board = internalB
				option.Piece = internalP.deepCopy()
				option.Score = internalB.evaluate()

				options = append(options, option)
			}

			return options
		}

		slideRightAndCreateOptions := func(b board, p *piece) []placementOption {

			internalP := p.deepCopy()
			internalB := b // this has no functionality other than making it more readable/clear
			options := []placementOption{}

			for internalP.moveRight(&internalB) {
				// try to drop the piece again cuz maybe it goes down even further after we moved left
				// and i think we only want to be at the lowest point
				internalP.drop(&internalB)
				option := placementOption{}

				option.Board = internalB
				option.Piece = internalP.deepCopy()
				option.Score = internalB.evaluate()

				options = append(options, option)
			}
			return options

		}

		internalP.drop(&internalB)

		slideLeftOptions := slideLeftAndCreateOptions(internalB, &internalP)
		slideRightOptions := slideRightAndCreateOptions(internalB, &internalP)

		options = append(options, slideLeftOptions...)
		options = append(options, slideRightOptions...)
		return options

	}

	internalPiece.wallKickLeft(&internalBoard)
	placementOptions = append(placementOptions, dropSlideAndCreateOptions(internalBoard, &internalPiece)...)

	for internalPiece.moveRight(&internalBoard) {

		placementOptions = append(placementOptions, dropSlideAndCreateOptions(internalBoard, &internalPiece)...)
	}
	return placementOptions

}

func (b board) findBestSpinMove(p piece) {

}

func (b board) findBestTeleportationMove(p piece) {

}
