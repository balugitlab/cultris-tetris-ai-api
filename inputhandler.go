package main

import (
	"bufio"
	"fmt"
	"os"
	"os/exec"
	"strings"
)

// Handles Input

func getInput() string {
	inputReader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter Input: ")
	input, _ := inputReader.ReadString('\n')
	// convert CRLF to LF
	input = strings.Replace(input, "\n", "", -1)
	return input
}

func getFluidInput(c chan string) {
	// gets input without enter presses

	// exec.Command("stty", "-F", "/dev/tty", "sane").Run()
	// reseting the terminal afaik only needed for linux

	// disable input buffering
	exec.Command("stty", "-F", "/dev/tty", "cbreak", "min", "1").Run()
	// do not display entered characters on the screen
	exec.Command("stty", "-F", "/dev/tty", "-echo").Run()
	var b = make([]byte, 1)

	for {
		os.Stdin.Read(b)
		c <- string(b)

	}
}
