package main

import (
	"math"
)

func getTileDistance(destinationTile, originTile *tile) float64 {

	x1 := originTile.XPosition
	y1 := originTile.YPosition

	x2 := destinationTile.XPosition
	y2 := destinationTile.YPosition

	x := x2 - x1
	y := y2 - y1

	xsquared := x * x
	ysquared := y * y

	xysquared := xsquared + ysquared

	sqrOfxy := math.Sqrt(float64(xysquared))

	return sqrOfxy

}

func getPieceDistance(destinationPiece, originPiece *piece) {

}

func findPath(destinationPiece, originPiece *piece, board *board) navigations {
	// returns the path from the origin to the destination in order
	// nil if no path has been found

	// internalBoard := *board
	//

	return nil

}

func getPathDistance(destinationPiece, originPiece *piece) {

	//get the avg of X and Y for the originPiece
	avgXOrigin := 0
	avgYOrigin := 0

	for _, tile := range originPiece.TilePositions {
		avgXOrigin += tile.XPosition
		avgYOrigin += tile.YPosition
	}
	avgXOrigin += originPiece.CenterTilePosition.XPosition
	avgYOrigin += originPiece.CenterTilePosition.YPosition

	avgXOrigin = avgXOrigin/len(originPiece.TilePositions) + 1 // +1 due to center tile
	avgYOrigin = avgYOrigin/len(originPiece.TilePositions) + 1

}
