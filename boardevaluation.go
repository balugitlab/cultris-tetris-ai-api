package main

import (
	"sort"
	"sync"
)

type hole struct {
	YPosition   int // the row
	XPosition   int // the column
	blocksAbove int // number of occupied tiles above hole
}

// byYPos implements sort.Interface based on the hole.YPosition field
// This allows us to sort the list of holes by their height (high to low)
type byYPos []hole

func (a byYPos) Len() int           { return len(a) }
func (a byYPos) Less(i, j int) bool { return a[i].YPosition < a[j].YPosition }
func (a byYPos) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }

func (b *board) evaluate() float64 {
	// Returns a value based on how ideal b.Board is

	// First we want to store original heuristics of the previous state for comparison
	origHeuristics := b.Heuristics

	// Then update the new heuristics based on the current state

	// Async appears to be slower for now for heuristics evaluation
	// var wg sync.WaitGroup
	// var aggregatedHeight int
	// var completeLines int
	// var holesCount int
	// var blocksAboveHole1 int
	// var blocksAboveHole2 int
	// var holes []hole
	// var bumpiness int
	//
	// wg.Add(4)
	// go b.aggregatedHeightAsync(&aggregatedHeight, &wg)
	// go b.completeLinesAsync(&completeLines, &wg)
	// go b.holes2Async(&holesCount, &blocksAboveHole1, &blocksAboveHole2, &holes, &wg)
	// go b.bumpinessAsync(&bumpiness, &wg)
	// wg.Wait()
	completeLines := b.completeLines()
	b.clearRows()
	holesCount, blocksAboveHole1, blocksAboveHole2, holes := b.holes2()
	rowTransAboveHole1 := len(b.rowTransAboveHole1(holes))
	combo := resolveCombo(b.Heuristics.Combo, completeLines)
	wellIndex, wellDepth, maxHeight, bumpiness := b.heightMetrics()

	b.Heuristics.Combo.comboCount = combo.comboCount
	b.Heuristics.Combo.comboTime = combo.comboTime

	b.Heuristics.maxHeight = maxHeight
	b.Heuristics.holesCount = holesCount
	b.Heuristics.blocksAboveHole1 = blocksAboveHole1
	b.Heuristics.blocksAboveHole2 = blocksAboveHole2
	b.Heuristics.completeLines = completeLines
	b.Heuristics.bumpiness = bumpiness
	b.Heuristics.rowTransAboveHole1 = rowTransAboveHole1
	b.Heuristics.wellDepth = wellDepth
	b.Heuristics.wellIndex = wellIndex

	return maxHeightWeight*float64(b.Heuristics.maxHeight) +
		completeLinesWeight*float64(b.Heuristics.completeLines) +
		holesWeight*float64(b.Heuristics.holesCount) +
		bumpinessWeight*float64(b.Heuristics.bumpiness) +
		blocksAboveHole1Weight*float64(b.Heuristics.blocksAboveHole1) +
		blocksAboveHole2Weight*float64(b.Heuristics.blocksAboveHole2) +
		rowTransAboveHole1Weight*float64(b.Heuristics.rowTransAboveHole1) +
		comboCountWeight*float64(b.Heuristics.Combo.comboCount) +
		comboTimeIncrementWeight*float64(b.Heuristics.Combo.comboTime-origHeuristics.Combo.comboTime) +
		wellWeight*float64(b.Heuristics.wellDepth) +
		float64(wellIndexWeight[b.Heuristics.wellIndex])
}

func (b *board) heightMetrics() (int, int, int, int) {

	heights := b.heights()
	wellIndex := 0
	wellHeight := 0
	maxHeight := 0
	for i := 0; i < len(heights); i++ {
		if heights[i] < heights[wellIndex] {
			wellIndex = i
			wellHeight = heights[i]
		}
		if heights[i] > maxHeight {
			maxHeight = heights[i]
		}
	}

	// array of differences bewteen adjacent column heights
	heightDiffs := make([]int, 0)
	var prev int
	if wellIndex == 0 {
		prev = 0
	} else {
		prev = 1
	}
	for x := 0; x < len(heights)-1; x++ {
		if x == wellIndex {
			continue
		}
		heightDiffs = append(heightDiffs, Abs(heights[prev]-heights[x]))
		prev = x

	}

	// Sum of height Diffs
	bumpiness := 0
	for i := range heightDiffs {
		bumpiness += heightDiffs[i]
	}

	wellDepth := 0
	for y := wellHeight; y < BOARDHEIGHT; y++ {
		// y iterates through the rows starting from the wellHeight
		for x := 0; x < BOARDWIDTH; x++ {
			// x iterates through columns left to Right
			if x != wellIndex && b.Board[y][x] != 0 {
				return wellIndex, wellDepth, maxHeight, bumpiness
			}
		}
		wellDepth++
	}

	return wellIndex, wellDepth, maxHeight, bumpiness
}

func (b *board) wellIndexDepth() (int, int) {
	// iterates through column heights and returns the index of the lowest height aka the wellIndex
	heights := b.heights()
	wellIndex := 0
	wellHeight := 0
	for i := 0; i < len(heights); i++ {
		if heights[i] < heights[wellIndex] {
			wellIndex = i
			wellHeight = heights[i]
		}
	}

	wellDepth := 0
	for y := wellHeight; y < BOARDHEIGHT; y++ {
		// y iterates through the rows starting from the wellHeight
		for x := 0; x < BOARDWIDTH; x++ {
			// x iterates through columns left to Right
			if x != wellIndex && b.Board[y][x] != 0 {
				return wellIndex, wellDepth
			}
		}
		wellDepth++
	}

	return wellIndex, wellDepth
}

func (b *board) rowTransAboveHole1(holes []hole) []int {
	if len(holes) > 0 {
		return b.rowTransitions(holes[0].YPosition - 1)
	}
	var empty []int
	return empty
}

func (b *board) rowTransitions(rowIndex int) []int {
	// Increment for each transitions between empty and solid cells exist when you move along a row

	var rowTrans []int
	row := b.Board[rowIndex]

	for i := 0; i < len(row)-1; i++ {
		if row[i] == 0 && row[i+1] == 1 {
			rowTrans = append(rowTrans, i)
		}
		if row[i] == 1 && row[i+1] == 0 {
			rowTrans = append(rowTrans, i)
		}
	}

	return rowTrans
}

func (b *board) aggregatedHeight() int {
	// This heuristic tells us how “high” a grid is. Starting at Index 0 e.g height 0 would be the lowest colum
	// To compute the aggregate height, we take the sum of the height of each column (the distance from the highest tile in each column to the bottom of the grid).

	aggregatedHeight := 0

	for i := 0; i < BOARDWIDTH; i++ {

		// finding the highest point
		for ii := 0; ii < BOARDHEIGHT; ii++ {

			if b.Board[ii][i] != 0 {
				aggregatedHeight += (BOARDHEIGHT - ii) // board is inversed thats why we subtract ii from the height
				break
			}
		}
	}

	return aggregatedHeight
}

func (b *board) aggregatedHeightAsync(aggregatedHeight *int, wg *sync.WaitGroup) {
	// This heuristic tells us how “high” a grid is. Starting at Index 0 e.g height 0 would be the lowest colum
	// To compute the aggregate height, we take the sum of the height of each column (the distance from the highest tile in each column to the bottom of the grid).

	for i := 0; i < BOARDWIDTH; i++ {

		// finding the highest point
		for ii := 0; ii < BOARDHEIGHT; ii++ {

			if b.Board[ii][i] != 0 {
				*aggregatedHeight += (BOARDHEIGHT - ii) // board is inversed thats why we subtract ii from the height
				break
			}
		}
	}

	wg.Done()
}

func (b *board) holes2() (int, int, int, []hole) {
	// Ideally this returns an array of the positions of all the holes
	// A hole is defined as an empty space such that there is at least one tile in the same column above it.

	holes := []hole{}
	var newHole hole

	for col := 0; col < BOARDWIDTH; col++ { // iterate through columns of board
		skipHole := true
		for colElement := 0; colElement < BOARDHEIGHT; colElement++ { //iterate through elements of column
			// setting skipHole to false when hitting the first occupied point
			if b.Board[colElement][col] != 0 {
				skipHole = false
				continue
			}
			if b.Board[colElement][col] == 0 && !skipHole {
				skipHole = true

				var blocksAbove int
				for row := colElement; row >= 0; row-- {
					if b.Board[row][col] != 0 {
						blocksAbove++
					}
				}
				newHole.XPosition = col
				newHole.YPosition = colElement
				newHole.blocksAbove = blocksAbove
				holes = append(holes, newHole)
			}
		}
	}
	// TODO:  rework how holes are added so that we can avoid having to sort
	sort.Sort(byYPos(holes))

	holeCount := len(holes)
	var blocksAboveHole1 int
	var blocksAboveHole2 int

	if holeCount < 1 {
	} else {
		blocksAboveHole1 = holes[0].blocksAbove
	}
	if holeCount < 2 {
	} else {
		blocksAboveHole2 = holes[1].blocksAbove
	}

	return holeCount, blocksAboveHole1, blocksAboveHole2, holes
}

func (b *board) holes2Async(holeCount *int, blocksAboveHole1 *int, blocksAboveHole2 *int, holes *[]hole, wg *sync.WaitGroup) {
	// Ideally this returns an array of the positions of all the holes
	// A hole is defined as an empty space such that there is at least one tile in the same column above it.

	var newHole hole
	for col := 0; col < BOARDWIDTH; col++ { // iterate through columns of board
		skipHole := true
		for colElement := 0; colElement < BOARDHEIGHT; colElement++ { //iterate through elements of column
			// setting skipHole to false when hitting the first occupied point
			if b.Board[colElement][col] != 0 {
				skipHole = false
				continue
			}
			if b.Board[colElement][col] == 0 && !skipHole {
				skipHole = true

				var blocksAbove int
				for row := colElement; row >= 0; row-- {
					if b.Board[row][col] != 0 {
						blocksAbove++
					}
				}
				newHole.XPosition = col
				newHole.YPosition = colElement
				newHole.blocksAbove = blocksAbove
				*holes = append(*holes, newHole)
			}
		}
	}
	// TODO:  rework how holes are added so that we can avoid having to sort
	sort.Sort(byYPos(*holes))

	*holeCount = len(*holes)

	if *holeCount < 1 {
	} else {
		*blocksAboveHole1 = (*holes)[0].blocksAbove
	}
	if *holeCount < 2 {
	} else {
		*blocksAboveHole2 = (*holes)[1].blocksAbove
	}
	wg.Done()
}

func (b *board) holes() int {
	// A hole is defined as an empty space such that there is at least one tile in the same column above it.

	holes := 0

	for col := 0; col < BOARDWIDTH; col++ {
		skipHole := true
		for colElement := 0; colElement < BOARDHEIGHT; colElement++ {

			// setting skipHole to false when hitting the first occupied point
			if b.Board[colElement][col] != 0 {
				skipHole = false
				continue
			}

			if b.Board[colElement][col] == 0 && !skipHole {
				skipHole = true
				holes++
			}
		}
	}

	return holes
}

func (b *board) completeLines() int {
	// It is simply the the number of complete lines in a grid. aka full rows
	lineCount := 0
	for _, row := range b.Board {

		isFull := true
		for _, rowItem := range row {
			if rowItem == 0 {
				isFull = false
				break
			}

		}
		if isFull {
			lineCount++
		}
	}
	return lineCount

}

func (b *board) completeLinesAsync(completeLines *int, wg *sync.WaitGroup) {
	// It is simply the the number of complete lines in a grid. aka full rows

	for _, row := range b.Board {

		isFull := true
		for _, rowItem := range row {
			if rowItem == 0 {
				isFull = false
				break
			}

		}
		if isFull {
			*completeLines++
		}
	}
	wg.Done()
}

func (b *board) bumpiness() int {
	// returns sum of absolute height differences of adjacent columns

	// array of differences bewteen adjacent column heights
	heightDiffs := make([]int, 0)

	// TODO: we are calling b.heights() multiple times for different things (maxHeight and bumpiness)
	heights := b.heights()
	for x := 0; x < len(heights)-1; x++ {
		heightDiffs = append(heightDiffs, Abs(heights[x]-heights[x+1]))
	}

	// Sum of height Diffs
	bumpiness := 0
	for i := range heightDiffs {
		bumpiness += heightDiffs[i]
	}
	return bumpiness
}

func (b *board) bumpinessAsync(bumpiness *int, wg *sync.WaitGroup) {
	// returns sum of absolute height differences of adjacent columns

	// absolute value helper
	abs := func(n int) int {
		if n < 0 {
			return -n
		}
		return n
	}

	// array of differences bewteen adjacent column heights
	heightDiffs := make([]int, 0)

	// TODO: we are calling b.heights() multiple times for different things (maxHeight and bumpiness)
	heights := b.heights()
	for x := 0; x < len(heights)-1; x++ {
		heightDiffs = append(heightDiffs, abs(heights[x]-heights[x+1]))
	}

	// Sum of height Diffs
	for i := range heightDiffs {
		*bumpiness += heightDiffs[i]
	}
	wg.Done()
}

func (b *board) column(columnIndex int) []int {
	// Returns array of a column by column index
	column := make([]int, 0)

	// iterate through board rows starting from bottom
	for i := len(b.Board) - 1; i >= 0; i-- {
		// append elements to column array
		column = append(column, b.Board[i][columnIndex])
	}
	return column
}

func (b *board) heights() []int {
	// Returns array of column heights
	heights := make([]int, BOARDWIDTH)

	// iterate through board columns. x = columnIndex
	for x := 0; x < len(b.Board[0]); x++ {
		column := b.column(x)
		// iterate through column array
		for j := len(column) - 1; j >= 0; j-- {
			// when we find non-0 element, we append that index as the height (index + 1) for that columnIndex
			if column[j] != 0 {
				heights[x] = j + 1
				break
			}
		}
	}
	return heights
}
