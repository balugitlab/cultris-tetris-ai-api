package main

import "testing"

func TestUpdateBoard(t *testing.T) {

	boardStruct := board{}
	boardStruct.update(1, 1, 1)

	if boardStruct.Board[1][1] != 1 {
		t.Error("Expected value 1 at y1 and x1")

	}
}

func TestAddGarbage(t *testing.T) {
	// Tests that we get false aka top out when we add garbage to a field with something in the top row
	boardStruct := board{}
	row0 := [10]int{0, 0, 0, 0, 0, 1, 1, 1, 0, 0}
	boardStruct.Board[0] = row0

	if boardStruct.addGarbage() {
		t.Error("Expected addGarbage to return false; Got", boardStruct.addGarbage())
	}

}
