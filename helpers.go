package main

func rotationMod(dividend, divisor int) int {
	// Mod that works with negative values

	return (dividend%divisor + divisor) % divisor
}

func removeEleFromSlice(slice navigations, ele int) navigations {
	// Order matters
	return append(slice[:ele], slice[ele+1:]...)
}

func removeEleFromSliceInPlace(slice navigations, i int) {
	// Order matters

	// Remove the element at index i from slice.
	copy(slice[i:], slice[i+1:])       // Shift a[i+1:] left one index.
	slice[len(slice)-1] = navigation{} // Erase last element (write zero value).
	slice = slice[:len(slice)-1]       // Truncate slice.

	return
}

func removeEleFromSliceMessy(s []int, i int) []int {
	// Order does not matter
	s[i] = s[len(s)-1]
	return s[:len(s)-1]
}

func Abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func sameTile(a, b tile) bool {
	if a.XPosition == b.XPosition && a.YPosition == b.YPosition {
		return true
	}
	// fmt.Println(a, "was not the same as", b)
	return false
}

func sameTiles(a, b []tile) bool {

	lena := len(a)
	lenb := len(b)

	if lena != lenb {
		// fmt.Println("a and b were different lengths")
		return false
	}

	visited := make([]bool, lenb)

	for i := 0; i < lena; i++ {
		tileofA := a[i]
		found := false
		for j := 0; j < lenb; j++ {
			if visited[j] {
				continue
			}
			tileofB := b[j]
			if sameTile(tileofA, tileofB) {
				visited[j] = true
				found = true
				break

			}

		}
		if !found {
			// fmt.Println(tileofA, "was not found in", b)
			return false
		}
	}
	// fmt.Println(a, "a was the same as b", b)
	return true
}

// stringInSlice is a helper for pixel matching
func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}
