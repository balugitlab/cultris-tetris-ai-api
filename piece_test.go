package main

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestDeepCopy(t *testing.T) {

	pieceType := "i"

	pieceStructOriginal := piece{}
	pieceStructOriginal.RawForm = generateRawPiece(pieceType)
	pieceStructOriginal.Type = pieceType
	for index := 0; index < 4; index++ {
		tile := new(tile)
		tile.XPosition = index
		tile.YPosition = 10

		pieceStructOriginal.TilePositions = append(pieceStructOriginal.TilePositions, *tile)
	}
	centerTileOrigina := tile{YPosition: 10, XPosition: 1}
	pieceStructOriginal.CenterTilePosition = centerTileOrigina

	copiedPieceStruct := pieceStructOriginal.deepCopy()

	// check if all values are the same

	if !cmp.Equal(copiedPieceStruct.RawForm, generateRawPiece(pieceType)) {
		t.Error("Copied Raw Form is not the same")

	}

	if copiedPieceStruct.Type != pieceType {
		t.Error("Copied Type is not the same")

	}

	if !cmp.Equal(copiedPieceStruct.TilePositions, pieceStructOriginal.TilePositions) {
		t.Error("Copied TilePositions are not the same")

	}

	if !cmp.Equal(copiedPieceStruct.CenterTilePosition, pieceStructOriginal.CenterTilePosition) {
		t.Error("Copied CenterTilePositions is not the same")

	}

	if !cmp.Equal(copiedPieceStruct.RotationIndex, pieceStructOriginal.RotationIndex) {
		t.Error("Copied RotationIndex is not the same")

	}

	// Changing the values in the copy now
	changedType := "o"
	changedRawForm := generateRawPiece(changedType)

	copiedPieceStruct.RawForm = changedRawForm

	if !cmp.Equal(copiedPieceStruct.RawForm, generateRawPiece(changedType)) {
		t.Error("Copied and Changed Raw Form is Wrong")

	}

	if cmp.Equal(copiedPieceStruct.RawForm, pieceStructOriginal.RawForm) {
		t.Error("RAW form change affected original piece")

	}

	// TODO: type test

	//changing TilePositions
	for i := range copiedPieceStruct.TilePositions {
		copiedPieceStruct.TilePositions[i].XPosition++
		copiedPieceStruct.TilePositions[i].YPosition++
	}

	if cmp.Equal(copiedPieceStruct.TilePositions, pieceStructOriginal.TilePositions) {
		t.Error("Copied TilePositions are linked")

	}

	copiedPieceStruct.CenterTilePosition.XPosition++
	copiedPieceStruct.CenterTilePosition.YPosition++

	if cmp.Equal(copiedPieceStruct.CenterTilePosition, pieceStructOriginal.CenterTilePosition) {
		t.Error("Copied CenterTilePositions are linked")

	}

	// TODO: rotationIndex
}
