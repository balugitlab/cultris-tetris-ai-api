package main

import (
	"fmt"
	"sync"
	"time"

	"github.com/go-vgo/robotgo"
	"github.com/spf13/viper"
)

func main() {

	//--------------------------------------------------------------------------------------------
	// // Interface wtih Cultris 2 aka deploy AI
	// This happens as a loop of:
	// 0) Initialize stuff
	// 1) internally calculate best move ->
	// 2) Execute best move using resolved key presses
	// 3) GUI (load field + next Piece) ->
	// 4) Return to 1

	// 0) Initialize Stuff //
	loadConfig()
	topX, topY, bottomX, bottomY := viper.GetInt("board.topX"), viper.GetInt("board.topY"), viper.GetInt("board.bottomX"), viper.GetInt("board.bottomY")
	previewX, previewY := viper.GetInt("preview.X"), viper.GetInt("preview.Y")
	ghostX, ghostY := viper.GetInt("ghost.X"), viper.GetInt("ghost.Y")

	var playerBoard board
	curP := generatePiece(getFirstGhost(ghostX, ghostY))
	fmt.Println("First Piece (ghost)", curP.Type)
	var moveList []string
	var preDownList []string
	var downList []string
	var postDownList []string
	maxBPM := 230.0 // this is the peak BPM - some moves may take longer to execute (e.g. a softdrop)
	minTimePerPiece := 1.0 / (maxBPM / 60.0) * float64(time.Second)

	for {
		bpmStart := time.Now()
		curP.spawnPieceOnBoard(&playerBoard)
		nextP := generatePiece(getPreview(previewX, previewY))
		fmt.Println("Current Piece:", curP.Type)
		fmt.Println("Next Piece:", nextP.Type)

		// 1) Calculate Best Move //
		_, _, bestPO := playerBoard.findBestHardorSoftDropMove(&curP, &nextP)
		if bestPO.Soft == false {
			moveList = bestPO.Piece.NavigationRecord.smartresolve()
		} else {
			preDownList, downList, postDownList = bestPO.Piece.NavigationRecord.internalSmartResolveSoft()

		}
		bestPO.Piece.NavigationRecord.resolve(&playerBoard, &curP)
		// playerBoard.evaluate()
		// fmt.Printf("b board post eval "+"%+v", board.Heuristics)
		fmt.Println("Solved Board:")
		playerBoard.showString()
		playerBoard.clearRows()

		// 2) Send Keypresses to execute moveList //
		if bestPO.Soft == false {
			for _, i := range moveList {
				fmt.Println(i)
				switch i {
				case "moveLeft":
					robotgo.KeyTap("left")
				case "moveRight":

					robotgo.KeyTap("right")
				case "rotateLeft":
					robotgo.KeyTap("q")

				case "rotateRight":
					robotgo.KeyTap("w")

				case "drop":
					robotgo.KeyTap("c")
				}
			}
		} else if bestPO.Soft == true {
			for _, i := range preDownList {
				fmt.Println(i)
				switch i {
				case "moveLeft":
					robotgo.KeyTap("left")

				case "moveRight":
					robotgo.KeyTap("right")

				case "rotateLeft":
					robotgo.KeyTap("q")

				case "rotateRight":
					robotgo.KeyTap("w")
				}
			}
			robotgo.KeyToggle("down", "down")
			robotgo.MilliSleep((len(downList) * 25))
			robotgo.KeyToggle("down", "up")

			for _, i := range postDownList {
				fmt.Println(i)
				switch i {
				case "moveLeft":
					robotgo.KeyTap("left")

				case "moveRight":
					robotgo.KeyTap("right")

				case "rotateLeft":
					robotgo.KeyTap("q")

				case "rotateRight":
					robotgo.KeyTap("w")

				case "drop":
					robotgo.KeyTap("c")
				}
			}

		}

		// 3) GUI //
		time.Sleep(100 * time.Millisecond) // Delay to avoid piecedrop animation in c2 and to throttle keyinputs below 400 pieces per minute
		newBoard := screenshotToField(topX, topY, bottomX, bottomY)
		fmt.Println("newBoard Board:")
		newBoard.clearRows()
		newBoard.showString()

		// Reset for next cycle
		playerBoard = newBoard
		curP = nextP

		// Throttle Speed to maxbpm
		elapsed := time.Since(bpmStart)
		if elapsed < time.Duration(minTimePerPiece) {
			// fmt.Println("sleeping for", time.Duration(minTimePerPiece)-elapsed)
			time.Sleep(time.Duration(minTimePerPiece) - elapsed)

		}

	}

	//--------------------------------------------------------------------------------------------

	// // API
	// startAPI()

	//--------------------------------------------------------------------------------------------
	// Deploy workers for fitness testing
	// var wg sync.WaitGroup
	//
	// for i := 0; i < 1; i++ {
	// 	// fmt.Println("Main: Starting worker", i)
	// 	wg.Add(1)
	// 	go worker(&wg, i)
	// }
	//
	// fmt.Println("Main: Waiting for workers to finish")
	// wg.Wait()
	// fmt.Println("Main: Completed")

	//--------------------------------------------------------------------------------------------

	// Manual testing using keyinput to terminal
	// p := generateRandomPiece()
	// b := &board{}
	//
	// p.spawnPieceOnBoard(b)
	// b.show()
	// fmt.Println("-----------------------")
	//
	// inputCh := make(chan string)
	// go getFluidInput(inputCh)
	// for {
	// 	oldState, err := terminal.MakeRaw(0)
	// 	if err != nil {
	// 		panic(err)
	// 	}
	// 	defer terminal.Restore(0, oldState)
	//
	// 	// input := getInput()
	// 	input := <-inputCh
	// 	switch input {
	//
	// 	case "e":
	// 		terminal.Restore(0, oldState) // for mac
	// 		fmt.Println("Exiting")
	// 		exec.Command("stty", "-F", "/dev/tty", "sane").Run() // reseting the terminal afaik only needed for linux
	// 		break
	//
	// 	case "k":
	// 		p.moveDown(b)
	// 	case "i":
	// 		p.moveUp(b)
	// 	case "j":
	// 		p.moveLeft(b)
	// 	case "l":
	// 		p.moveRight(b)
	// 	case "c":
	// 		p.drop(b)
	// 		b.clearRows()
	// 		p = generatePiece("i")
	// 		p.spawnPieceOnBoard(b)
	// 	case "q":
	// 		p.rotatePiece(b, false)
	// 	case "w":
	// 		p.rotatePiece(b, true)
	//
	// 	}
	//
	// 	terminal.Restore(0, oldState)
	// 	b.show()
	// 	fmt.Println("-----------------------")
	//
	// }
	// 	os.Exit(1)
}

func worker(wg *sync.WaitGroup, id int) {
	defer wg.Done()

	fmt.Printf("Worker %v: Started\n", id)
	fmt.Println("Worker", id, "Score:", fitnessSurvivor())
}
