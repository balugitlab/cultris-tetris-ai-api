package main

import (
	"errors"
	"fmt"
	"image/color"
	"math"
	"sync"

	"github.com/go-vgo/robotgo"
	"github.com/spf13/viper"
)

func pixelIsInRange(basecolor, color string, colorRange float64) bool {

	rGBbasecolor, _ := parseHexColor(basecolor)
	rGBcolor, _ := parseHexColor(color)

	r := math.Abs(float64(rGBbasecolor.R) - float64(rGBcolor.R))
	g := math.Abs(float64(rGBbasecolor.G) - float64(rGBcolor.G))
	b := math.Abs(float64(rGBbasecolor.B) - float64(rGBcolor.B))

	max := math.Max(r, g)
	max = math.Max(max, b)

	if max > colorRange {
		return false

	}

	return true

}

func loadConfig() {
	viper.SetConfigName("config") // name of config file (without extension)
	viper.AddConfigPath(".")      //look for config in the working directory

	err := viper.ReadInConfig() // Find and read the config file
	if err != nil {             // Handle errors reading the config file
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}
}

func getFieldPoints() (topX, topY, bottomX, bottomY int) {

	// returns the top most left corner and the bottom most right one

	// getting the top most left point
	fmt.Println("Move your cursor to the top most left point of the playing field and pres enter")
	fmt.Scanln()
	topX, topY = robotgo.GetMousePos()

	// getting the top most right point
	fmt.Println("Move your cursor to the bottom most right point of the playing field and pres enter")
	fmt.Scanln()
	bottomX, bottomY = robotgo.GetMousePos()

	return
}

func parseManager(topX, topY, bottomX, bottomY int, c chan [BOARDHEIGHT][BOARDWIDTH]int) {

	go func() {
		for {
			c <- parseFieldToArray(topX, topY, bottomX, bottomY)
		}
	}()

}

func parseFieldToArray(topX, topY, bottomX, bottomY int) [BOARDHEIGHT][BOARDWIDTH]int {

	//get values
	emptyTile := viper.GetString("emtpyTileHexColor")

	field := [BOARDHEIGHT][BOARDWIDTH]int{}
	width := bottomX - topX
	// height := bottomY - topY
	tileLength := width / BOARDWIDTH

	var wg sync.WaitGroup

	for col := 0; col < BOARDHEIGHT; col++ {

		wg.Add(1)

		go func(col int) {
			for row := 0; row < BOARDWIDTH; row++ {
				yPos := topY + (tileLength * (col + 1)) - (tileLength / 2)

				xPos := topX + (tileLength * (row + 1)) - (tileLength / 2)
				color := robotgo.GetPixelColor(xPos, yPos)

				if color == emptyTile {
					field[col][row] = 0
					continue
				}

				field[col][row] = 1

			}
			wg.Done()
		}(col)

	}

	wg.Wait()
	return field

}

func screenshotToField(topX, topY, bottomX, bottomY int) board {
	board := board{}
	// get values
	emptyTile := viper.GetString("emtpyTileHexColor")
	ghostsColors := []string{viper.GetString("iPiece.hexGhostColor"), viper.GetString("oPiece.hexGhostColor"), viper.GetString("zPiece.hexGhostColor"), viper.GetString("sPiece.hexGhostColor"), viper.GetString("lPiece.hexGhostColor"), viper.GetString("jPiece.hexGhostColor"), viper.GetString("tPiece.hexGhostColor")}

	// initialize boundaries and tile sizes
	field := [BOARDHEIGHT][BOARDWIDTH]int{}
	width := bottomX - topX
	tileLength := (width / BOARDWIDTH) * 2 //bitmap width height is x2 the width and height of x and y

	// capture the screen as a bitmap
	bitmap := robotgo.CaptureScreen(topX, topY, bottomX-topX, bottomY-topY)
	defer robotgo.FreeBitmap(bitmap)

	// iterate through cells to parse to a field
	for row := BOARDHEIGHT - 1; row > 2; row-- {
		for col := 0; col < BOARDWIDTH; col++ {
			yPos := topY + (tileLength * (row)) - (5 * tileLength / 2) // not 100% sure why we shift up by 2.5 for the bitmap
			xPos := topX + (tileLength * (col)) - (tileLength / 2)

			color := robotgo.GetColors(bitmap, xPos, yPos)

			// if the cell pixel is an empty tile or a ghost, make the cell a 0, else a 1
			if color == emptyTile || stringInSlice(color, ghostsColors) {
				field[row][col] = 0
				board.update(row, col, 0)
				continue
			}
			field[row][col] = 1
			board.update(row, col, 1)
		}
	}
	// fmt.Printf("b board post eval "+"%+v", bitmap)
	// robotgo.SaveBitmap(bitmap, "test.png")

	// for col, i := range field {
	// 	for row, j := range i {
	// 		if j == 1 {
	// 			board.update(col, row, 1)
	// 		} else if j == 0 {
	// 			board.update(col, row, 0)
	// 		}
	// 	}
	//
	// }
	return board
}

func getPreview(x, y int) string {

	color := robotgo.GetPixelColor(x, y)
	var piece string

	switch {
	case color == viper.GetString("iPiece.hexColor"):
		piece = "i"
	case color == viper.GetString("jPiece.hexColor"):
		piece = "j"
	case color == viper.GetString("lPiece.hexColor"):
		piece = "l"
	case color == viper.GetString("oPiece.hexColor"):
		piece = "o"
	case color == viper.GetString("tPiece.hexColor"):
		piece = "t"
	case color == viper.GetString("sPiece.hexColor"):
		piece = "s"
	case color == viper.GetString("zPiece.hexColor"):
		piece = "z"
	default:
		err := "Did not find matching hexColor for color" + color
		panic(fmt.Errorf("fatal error config file: %s \n", err))
	}
	return piece
}

func getFirstGhost(x, y int) string {

	color := robotgo.GetPixelColor(x, y)
	var piece string

	switch {
	case color == viper.GetString("iPiece.hexGhostColor"):
		piece = "i"
	case color == viper.GetString("jPiece.hexGhostColor"):
		piece = "j"
	case color == viper.GetString("lPiece.hexGhostColor"):
		piece = "l"
	case color == viper.GetString("oPiece.hexGhostColor"):
		piece = "o"
	case color == viper.GetString("tPiece.hexGhostColor"):
		piece = "t"
	case color == viper.GetString("sPiece.hexGhostColor"):
		piece = "s"
	case color == viper.GetString("zPiece.hexGhostColor"):
		piece = "z"
	default:
		err := "Did not find matching hexColor for color" + color
		panic(fmt.Errorf("fatal error config file: %s \n", err))
	}
	return piece
}

func takeScreenShot(topX, topY, bottomX, bottomY int) {

	bitmap := robotgo.CaptureScreen(topX, topY, bottomX-topX, bottomY-topY)
	defer robotgo.FreeBitmap(bitmap)

	robotgo.SaveBitmap(bitmap, "test.png")

}

func mousPosLoop() {
	for {
		topX, topY := robotgo.GetMousePos()
		fmt.Println(topX, topY)
	}
}

func pixelColorLoop() {

	for {
		x, y := robotgo.GetMousePos()

		color := robotgo.GetPixelColor(x, y)
		fmt.Println("HEXcolor---- ", color)

	}
}

// Parse - parse hex string to image/color
func parseHexColor(hex string) (c color.RGBA, err error) {
	// got this parsing code from here https://stackoverflow.com/a/54200713/1723695
	hex = "#" + hex

	var errInvalidFormat = errors.New("invalid format")

	c.A = 0xff

	if hex[0] != '#' {
		return c, errInvalidFormat
	}

	hexToByte := func(b byte) byte {
		switch {
		case b >= '0' && b <= '9':
			return b - '0'
		case b >= 'a' && b <= 'f':
			return 10 + b - 'a'
		case b >= 'A' && b <= 'F':
			return 10 + b - 'A'
		}
		err = errInvalidFormat
		return 0
	}

	switch len(hex) {
	case 7:
		c.R = (hexToByte(hex[1]) << 4) + hexToByte(hex[2])
		c.G = (hexToByte(hex[3]) << 4) + hexToByte(hex[4])
		c.B = (hexToByte(hex[5]) << 4) + hexToByte(hex[6])
	case 4:
		c.R = hexToByte(hex[1]) * 17
		c.G = hexToByte(hex[2]) * 17
		c.B = hexToByte(hex[3]) * 17
	default:
		err = errInvalidFormat
	}
	return
}
