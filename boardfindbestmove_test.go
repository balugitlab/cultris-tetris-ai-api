package main

import (
	"fmt"
	"testing"
)

func TestGetIdealPlacement(t *testing.T) {

	boardStruct := &board{}
	row9 := [10]int{0, 0, 0, 0, 0, 1, 1, 1, 0, 0}
	row10 := [10]int{0, 0, 0, 0, 1, 1, 1, 1, 1, 0}
	row11 := [10]int{0, 0, 0, 1, 1, 1, 1, 1, 1, 1}
	row12 := [10]int{0, 0, 0, 1, 1, 1, 1, 1, 1, 1}
	row13 := [10]int{0, 0, 0, 1, 1, 1, 1, 1, 1, 1}
	row14 := [10]int{0, 0, 0, 1, 1, 1, 1, 1, 1, 1}
	row15 := [10]int{0, 0, 0, 1, 1, 1, 1, 1, 1, 1}
	row16 := [10]int{0, 0, 0, 0, 0, 1, 1, 1, 1, 1}
	row17 := [10]int{1, 1, 1, 1, 0, 1, 1, 1, 1, 1}
	row18 := [10]int{1, 1, 1, 1, 0, 1, 1, 1, 1, 1}
	row19 := [10]int{1, 1, 1, 0, 0, 1, 1, 1, 1, 1}

	boardStruct.Board[9] = row9
	boardStruct.Board[10] = row10
	boardStruct.Board[11] = row11
	boardStruct.Board[12] = row12
	boardStruct.Board[13] = row13
	boardStruct.Board[14] = row14
	boardStruct.Board[15] = row15
	boardStruct.Board[16] = row16
	boardStruct.Board[17] = row17
	boardStruct.Board[18] = row18
	boardStruct.Board[19] = row19
	p := generatePiece("i")
	p.spawnPieceOnBoard(boardStruct)
	bestpOption := boardStruct.getIdealPlacement(&p)

	if bestpOption.Board.Board[18][4] != 2 {
		t.Error("Get Ideal Placement chose a bad spot for i piece:")
		fmt.Println("Initial board:")
		boardStruct.show()
		fmt.Println("Chosen placement:")
		bestpOption.Board.show()

	}
}

func BenchmarkFindBestHardDrop(b *testing.B) {
	for n := 0; n < b.N; n++ {
		boardStruct := &board{}
		for k := 0; k < 8; k++ {
			boardStruct.addGarbage()
		}
		p := generateRandomPiece()
		nextP := generateRandomPiece()
		p.spawnPieceOnBoard(boardStruct)
		boardStruct.findBestHardDropMove(&p, &nextP)
	}
}
