package main

import (
	"testing"
)

// func TestEvaluate(t *testing.T) {
//
// }

func TestWellIndex(t *testing.T) {
	boardStruct := board{}
	row9 := [10]int{0, 0, 0, 0, 0, 1, 1, 1, 0, 0}
	row10 := [10]int{0, 0, 0, 0, 0, 1, 1, 1, 1, 0}
	row11 := [10]int{0, 0, 0, 0, 0, 1, 1, 1, 1, 1}
	row12 := [10]int{0, 0, 0, 0, 0, 1, 1, 1, 1, 1}
	row13 := [10]int{0, 0, 0, 0, 0, 1, 1, 1, 1, 1}
	row14 := [10]int{0, 0, 0, 0, 0, 1, 1, 1, 1, 1}
	row15 := [10]int{0, 0, 0, 0, 1, 1, 1, 1, 1, 1}
	row16 := [10]int{0, 0, 0, 0, 1, 1, 1, 1, 1, 1}
	row17 := [10]int{1, 1, 0, 0, 0, 1, 1, 1, 1, 1}
	row18 := [10]int{1, 1, 1, 0, 1, 1, 1, 1, 1, 1}
	row19 := [10]int{1, 1, 1, 1, 0, 1, 1, 1, 1, 1}

	boardStruct.Board[9] = row9
	boardStruct.Board[10] = row10
	boardStruct.Board[11] = row11
	boardStruct.Board[12] = row12
	boardStruct.Board[13] = row13
	boardStruct.Board[14] = row14
	boardStruct.Board[15] = row15
	boardStruct.Board[16] = row16
	boardStruct.Board[17] = row17
	boardStruct.Board[18] = row18
	boardStruct.Board[19] = row19
	wellIndex, wellDepth := boardStruct.wellIndexDepth()
	if wellIndex != 3 {
		boardStruct.showString()
		t.Error("board has a wellIndex of:", wellIndex)
	}
	if wellDepth != 8 {
		boardStruct.showString()
		t.Error("board has a wellDepth of:", wellDepth)
	}

}

func TestAggregatedHeight(t *testing.T) {

	boardStruct := board{}

	if boardStruct.aggregatedHeight() > 0 {
		t.Error("Empty board has and aggregatedHeight of:", boardStruct.aggregatedHeight())

	}

	boardStruct.Board[0][0] = 1

	if boardStruct.aggregatedHeight() != 20 {
		t.Error(" aggregatedHeight should be 20:", boardStruct.aggregatedHeight())

	}

	boardStruct.Board[0][1] = 1

	if boardStruct.aggregatedHeight() != 40 {
		t.Error(" aggregatedHeight should be 40:", boardStruct.aggregatedHeight())

	}

	// TODO: test for holes

}

func TestHoles(t *testing.T) {

	boardStruct := board{}
	rowIndex := 1

	boardStruct.Board[19][rowIndex] = 1
	boardStruct.Board[14][rowIndex] = 1

	if boardStruct.holes() != 1 {
		t.Error("Expected One hole got:", boardStruct.holes())

	}

}

//
func TestCompleteLines(t *testing.T) {

	boardStruct := board{}
	if boardStruct.completeLines() > 0 {
		t.Error("Empty board has :", boardStruct.completeLines(), "completelines")

	}

	boardStruct.Board[0][0] = 1
	boardStruct.Board[0][1] = 1
	boardStruct.Board[0][2] = 1
	boardStruct.Board[0][3] = 1
	boardStruct.Board[0][4] = 1
	boardStruct.Board[0][5] = 1
	boardStruct.Board[0][6] = 1
	boardStruct.Board[0][7] = 1
	boardStruct.Board[0][8] = 1
	if boardStruct.completeLines() > 0 {
		t.Error("Nearly completed row counted")

	}

	boardStruct.Board[0][9] = 1
	if boardStruct.completeLines() != 1 {
		t.Error("We should have 1 completed line but we have:", boardStruct.completeLines())
	}
}

func TestBumpiness(t *testing.T) {

	boardStruct := board{}

	if boardStruct.bumpiness() != 0 {
		t.Error("Empty board has a bumpines of:", boardStruct.bumpiness())

	}

	boardStruct.Board[17][0] = 1
	boardStruct.Board[15][1] = 1
	boardStruct.Board[15][2] = 1
	boardStruct.Board[15][3] = 1
	boardStruct.Board[14][4] = 1
	boardStruct.Board[14][5] = 1
	boardStruct.Board[15][6] = 1
	boardStruct.Board[16][7] = 1
	boardStruct.Board[16][8] = 1
	boardStruct.Board[15][9] = 1

	if boardStruct.bumpiness() != 6 {
		t.Error("Bumpines should be 6 have:", boardStruct.bumpiness())

	}

}

func TestHeuristics(t *testing.T) {
	boardStruct := board{}

	row9 := [10]int{0, 0, 0, 0, 0, 1, 1, 1, 0, 0}
	row10 := [10]int{0, 0, 0, 0, 1, 1, 1, 1, 1, 0}
	row11 := [10]int{0, 0, 0, 1, 1, 1, 1, 1, 1, 1}
	row12 := [10]int{0, 0, 0, 1, 1, 1, 1, 1, 1, 1}
	row13 := [10]int{0, 0, 0, 1, 1, 1, 1, 1, 1, 1}
	row14 := [10]int{0, 0, 0, 1, 1, 1, 1, 1, 1, 1}
	row15 := [10]int{0, 0, 0, 1, 1, 1, 1, 1, 1, 1}
	row16 := [10]int{0, 0, 0, 0, 0, 1, 1, 1, 1, 1}
	row17 := [10]int{1, 1, 1, 1, 0, 1, 1, 1, 1, 1}
	row18 := [10]int{1, 1, 1, 1, 0, 1, 1, 1, 1, 1}
	row19 := [10]int{1, 1, 1, 0, 0, 1, 1, 1, 1, 1}

	boardStruct.Board[9] = row9
	boardStruct.Board[10] = row10
	boardStruct.Board[11] = row11
	boardStruct.Board[12] = row12
	boardStruct.Board[13] = row13
	boardStruct.Board[14] = row14
	boardStruct.Board[15] = row15
	boardStruct.Board[16] = row16
	boardStruct.Board[17] = row17
	boardStruct.Board[18] = row18
	boardStruct.Board[19] = row19

	if boardStruct.holes() != 3 {
		t.Error("holes should be 3 have:", boardStruct.holes())
	}

	holes2Len, blocksAboveHole1, blocksAboveHole2, holes := boardStruct.holes2()
	if holes2Len != 3 {
		boardStruct.show()
		t.Error("holes2_len should be 3 have:", holes2Len)
	}

	if blocksAboveHole1 != 5 {
		// boardStruct.show()
		t.Error("blocksAboveHole1 should be 5, have:", blocksAboveHole1)
	}
	if blocksAboveHole2 != 6 {
		// boardStruct.show()
		t.Error("blocksAboveHole2 should be 6, have:", blocksAboveHole2)
	}

	if len(boardStruct.rowTransAboveHole1(holes)) != 1 {
		boardStruct.show()
		t.Error("rowTransAboveHole1(holes) should be [2], len 1:", boardStruct.rowTransAboveHole1(holes))
	}

	wellIndex, wellDepth, maxHeight, bumpiness := boardStruct.heightMetrics()
	if bumpiness != 9 {
		boardStruct.show()
		t.Error("bumpiness should be 9 have:", bumpiness)
	}
	if maxHeight != 11 {
		t.Error("maxHeight should be 11 have:", maxHeight)
	}
	if wellIndex != 0 {
		t.Error("wellIndex should be 0 have:", wellIndex)
	}
	if wellDepth != 9 {
		boardStruct.show()
		t.Error("wellDepth should be 9 have:", wellDepth)
	}

}
