package main

import (
	"math"
	"sync"
)

// the first thing we want is given a node (placementoption), we shuld be able to generate the neighbors
// A neighbor is a placementOptino that is exactly 1 action away from a current placementOptions
// An action is a combination of a rotation (none, 1 rotate, 2 rotate, 3 rotate - optimized for symetric pieces) and a slide (none, left, right, down)
func (b *board) getNeigbors(p *piece) []placementOption {

	var placementOptions []placementOption
	internalPiece := p.deepCopy()
	internalBoard := *b

	internalPieceZero := p.deepCopy()
	zeroRotationPlacementOptions := internalBoard.getNeighborsCurrRotation(&internalPieceZero)
	placementOptions = append(placementOptions, zeroRotationPlacementOptions...)
	internalPieceZero.remove(&internalBoard)

	if internalPiece.Type != "o" {
		// rotate piece CW
		internalPieceOne := p.deepCopy()
		internalPieceOne.rotatePiece(&internalBoard, true)

		firstRotationPlacementOptions := internalBoard.getNeighborsCurrRotation(&internalPieceOne)
		placementOptions = append(placementOptions, firstRotationPlacementOptions...)
		internalPieceOne.remove(&internalBoard)

		if internalPiece.Type != "s" && internalPiece.Type != "z" && internalPiece.Type != "i" {
			// rotate piece 180
			internalPieceTwo := p.deepCopy()
			internalPieceTwo.rotatePiece(&internalBoard, true)
			internalPieceTwo.rotatePiece(&internalBoard, true)

			secondRotationPlacementOptions := internalBoard.getNeighborsCurrRotation(&internalPieceTwo)
			placementOptions = append(placementOptions, secondRotationPlacementOptions...)
			internalPieceTwo.remove(&internalBoard)

			// rotate piece CCW
			internalPieceThree := p.deepCopy()
			internalPieceThree.rotatePiece(&internalBoard, false)

			thirdRotationPlacementOptions := internalBoard.getNeighborsCurrRotation(&internalPieceThree)
			placementOptions = append(placementOptions, thirdRotationPlacementOptions...)
			internalPieceThree.remove(&internalBoard)

		}
	}
	return placementOptions
}

func (b *board) getNeigborsAsync(p *piece) []placementOption {
	var placementOptions []placementOption

	internalPiece := p.deepCopy()
	internalP := internalPiece.deepCopy()
	internalBoard := *b

	var wg sync.WaitGroup
	var zeroRotationPlacementOptions []placementOption
	var firstRotationPlacementOptions []placementOption
	var secondRotationPlacementOptions []placementOption
	var thirdRotationPlacementOptions []placementOption

	wg.Add(1)
	go internalBoard.getNeighborsCurrRotationAsync(&internalP, &zeroRotationPlacementOptions, &wg)

	if internalPiece.Type != "o" {
		internalPiece.rotatePiece(&internalBoard, true)
		internalP = internalPiece.deepCopy()

		wg.Add(1)
		go internalBoard.getNeighborsCurrRotationAsync(&internalP, &firstRotationPlacementOptions, &wg)

		if internalPiece.Type != "s" && internalPiece.Type != "z" && internalPiece.Type != "i" {
			// rotate piece
			internalPiece.rotatePiece(&internalBoard, true)
			internalP = internalPiece.deepCopy()

			wg.Add(1)
			go internalBoard.getNeighborsCurrRotationAsync(&internalP, &secondRotationPlacementOptions, &wg)

			// rotate piece
			internalPiece.rotatePiece(&internalBoard, true)
			internalP = internalPiece.deepCopy()

			wg.Add(1)
			go internalBoard.getNeighborsCurrRotationAsync(&internalP, &thirdRotationPlacementOptions, &wg)

		}
	}

	wg.Wait()
	placementOptions = append(placementOptions, zeroRotationPlacementOptions...)
	if internalPiece.Type != "o" {
		placementOptions = append(placementOptions, firstRotationPlacementOptions...)
		if internalPiece.Type != "s" && internalPiece.Type != "z" && internalPiece.Type != "i" {
			placementOptions = append(placementOptions, secondRotationPlacementOptions...)

			placementOptions = append(placementOptions, thirdRotationPlacementOptions...)
		}
	}

	return placementOptions
}

func isDuplicateOption(option placementOption, options []placementOption) bool {
	// checks if a generated option is already existing in a list of placement options
	// for the option and each option in options, we create a tiles list that contians the tilepositions + centerTilePosition
	// then compare them using the helper sameTilesSlice which checks for equality without caring for order
	// it is also possible to use the reflect.DeepEqual function, though the sameTilesSlice seems marginally faster
	duplicate := false
	internalP := option.Piece.deepCopy()
	internalTiles := append(internalP.TilePositions, internalP.CenterTilePosition)

	for i := 0; i < len(options); i++ {
		comparisonTiles := append(options[i].Piece.TilePositions, options[i].Piece.CenterTilePosition)
		duplicate = sameTiles(internalTiles, comparisonTiles)
		// duplicate = reflect.DeepEqual(internalTiles, comparisonTiles)

		if duplicate {
			return true
		}
	}
	return duplicate
}

func (b *board) getNeighborsCurrRotation(p *piece) []placementOption {

	internalPiece := p.deepCopy()
	internalBoard := *b
	placementOptions := []placementOption{}

	moveAndCreateOption := func(b board, p *piece, s string) (bool, placementOption) {
		// given a piece, attempts to move it in the direction of s
		// if the move is succesful, succMove is true
		// if succMove is true and the piece is not floating, we return true, and a new placementoption
		// else if succMove is false or the piece is floating, we return false and a placementOption which score negative infinity

		internalP := p.deepCopy()
		internalB := b // this has no functionality other than making it more readable/clear
		option := placementOption{}
		var succMove bool

		switch s {
		case "N":
			succMove = true
		case "Left":
			succMove = internalP.moveLeft(&internalB)
		case "Right":
			succMove = internalP.moveRight(&internalB)
		case "Down":
			succMove = internalP.moveDown(&internalB)
			// case "Kickleft":
			// 	internalP.wallKickLeft(&internalB)
			// succMove = true
			// case "Kickright":
			// 	internalP.wallKickRight(&internalB)
			// succMove = true
		}
		// internalP.drop(&internalB)

		if succMove && !internalP.isFloating(&internalB) {
			option.Board = internalB
			option.Piece = internalP.deepCopy()
			option.Score = internalB.evaluate()
			internalP.remove(&internalB)
			return succMove, option

		} else {
			option.Board = internalB
			option.Piece = internalP.deepCopy()
			option.Score = math.Inf(-8)
			internalP.remove(&internalB)
			return false, option
		}
	}

	// slideMoves := []string{"N", "Left", "Right", "Down", "Kickleft", "Kickright"} // adding kickLeft/Right here would create more slide moves
	slideMoves := []string{"N", "Left", "Right", "Down"}

	// iterate through slideMoves and create an option for each move as long as it is
	// 1) useful (see moveAndCreateOption())
	// 2) unique (not already in the list of placement options - see isDuplicateOption())
	for i := 0; i < len(slideMoves); i++ {
		useful, potentialOption := moveAndCreateOption(internalBoard, &internalPiece, slideMoves[i])
		duplicate := isDuplicateOption(potentialOption, placementOptions)
		if useful && !duplicate {
			placementOptions = append(placementOptions, potentialOption)
		}
	}
	return placementOptions
}

func (b *board) getNeighborsCurrRotationAsync(p *piece, placementOptions *[]placementOption, wg *sync.WaitGroup) {

	internalPiece := p.deepCopy()
	internalBoard := *b

	moveAndCreateOption := func(b board, p *piece, s string) (bool, placementOption) {
		// given a piece, attempts to move it in the direction of s
		// if the move is succesful, succMove is true
		// if succMove is true and the piece is not floating, we return true, and a new placementoption
		// else if succMove is false or the piece is floating, we return false and a placementOption which score negative infinity

		internalP := p.deepCopy()
		internalB := b // this has no functionality other than making it more readable/clear
		option := placementOption{}
		var succMove bool

		switch s {
		case "N":
			succMove = true
		case "Left":
			succMove = internalP.moveLeft(&internalB)
		case "Right":
			succMove = internalP.moveRight(&internalB)
		case "Down":
			succMove = internalP.moveDown(&internalB)
			// case "Kickleft":
			// 	internalP.wallKickLeft(&internalB)
			// case "Kickright":
			// 	internalP.wallKickRight(&internalB)
		}
		internalP.drop(&internalB)

		if succMove && !internalP.isFloating(&internalB) {
			option.Board = internalB
			option.Piece = internalP.deepCopy()
			option.Score = internalB.evaluate()
			option.Soft = true
			internalP.remove(&internalB)
			return succMove, option

		} else {
			option.Board = internalB
			option.Piece = internalP.deepCopy()
			option.Score = math.Inf(-8)
			internalP.remove(&internalB)
			return false, option
		}
	}

	// slideMoves := []string{"Left", "Right", "Down", "Kickleft", "Kickright"} // adding kickLeft/Right here would create more slide moves
	slideMoves := []string{"N", "Left", "Right", "Down"}

	// iterate through slideMoves and create an option for each move as long as it is
	// 1) useful (see moveAndCreateOption())
	// 2) unique (not already in the list of placement options - see isDuplicateOption())
	for i := 0; i < len(slideMoves); i++ {
		useful, potentialOption := moveAndCreateOption(internalBoard, &internalPiece, slideMoves[i])
		duplicate := isDuplicateOption(potentialOption, *placementOptions)
		if useful && !duplicate {
			*placementOptions = append(*placementOptions, potentialOption)
		}
	}
	wg.Done()
}

// getSpins gets neighbors (positions that are 1 action away) for each item in a list of placemenOptions
// it appends the spinmoves to the given spin list only if they are new positiosn (not duplicates)
func getSpins(options []placementOption) []placementOption {
	placementOptions := []placementOption{}

	for i := 0; i < len(options); i++ {
		// it is quesitonable whether we need to make these copies here but appears to not effect efficiency...
		// internalBoard := options[i].Board
		// internalPiece := options[i].Piece.deepCopy()
		// placementOptions = append(placementOptions, internalBoard.getNeigborsAsync(&internalPiece)...)
		// placementOptions = append(placementOptions, internalBoard.getNeigbors(&internalPiece)...)

		placementOptions = append(placementOptions, options[i].Board.getNeigborsAsync(&options[i].Piece)...)
		// placementOptions = append(placementOptions, options[i].Board.getNeigbors(&options[i].Piece)...)
	}
	for i := 0; i < len(placementOptions); i++ {
		if !isDuplicateOption(placementOptions[i], options) {
			options = append(options, placementOptions[i])
		}
	}

	return options

}

func (b *board) findBestHardorSoftDropMove(p *piece, nextPiece *piece) (float64, navigations, placementOption) {
	// returns the the score of the best move with the instructions on how to get there
	// in this case we get the harddrop placement options, then we append to them, the spin options using getSpins
	navigationInstuctions := navigations{}
	var bestPO placementOption
	var bestScore float64

	internalP := p.deepCopy()
	internalNextP := nextPiece.deepCopy()
	internalB := *b

	// get the movement options for the current piece
	placementOptionsCurPiece := internalB.generateHardDropPlacementOptions(&internalP)
	placementOptionsCurPiece = getSpins(placementOptionsCurPiece)

	bestScore = math.Inf(-8)
	for _, plo := range placementOptionsCurPiece {
		// reset the internalNextP and spawn it on the current plo (placementoption) board
		internalNextP = nextPiece.deepCopy()
		internalNextP.spawnPieceOnBoard(&plo.Board)

		// get the movement options for the next piece
		placementOptionsNextPiece := plo.Board.generateHardDropPlacementOptions(&internalNextP)
		placementOptionsNextPiece = getSpins(placementOptionsNextPiece)

		for _, nextplo := range placementOptionsNextPiece {
			// debug stuff below
			// fmt.Println(i, "Current", plo.Piece.Type, "Next", nextplo.Piece.Type, nextplo.Score)
			// fmt.Println("currpiece tilepos:", plo.Piece.TilePositions)
			// fmt.Println("currpiece centerpos:", plo.Piece.CenterTilePosition)
			//
			// fmt.Println("nextpiece tilepos:", nextplo.Piece.TilePositions)
			// fmt.Println("nextpiece centerpos:", nextplo.Piece.CenterTilePosition)

			// nextplo.Board.showString()

			// if the nextplo score is better than any so far, assign it as the best score and navigationInstructions for the current piece
			if nextplo.Score > bestScore {
				bestScore = nextplo.Score
				bestPO = plo
				navigationInstuctions = plo.Piece.NavigationRecord
			}
		}
	}

	return bestScore, navigationInstuctions, bestPO
}
