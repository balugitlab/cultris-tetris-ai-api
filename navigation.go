package main

import "fmt"

type navigations []navigation

type navigation struct {
	movement
	rotation
}

type movement struct {
	MoveLeft, WallKickLeft, WallKickRight, MoveRight, MoveDown, Drop bool
}

type rotation struct {
	RotateLeft, RotateRight, Rotate180 bool
}

func (ns *navigations) add(v ...interface{}) {

	// Appends the values/items of v type navigation or navigations to ns

	for _, value := range v {

		switch n := value.(type) {

		case navigations:
			*ns = append(*ns, n...)

		case navigation:
			*ns = append(*ns, n)

		default:
			fmt.Println("ERROR IN ADD TO NAVIGATIONS")

		}
	}

}

func (ns *navigations) remove(index int) {

}

func (ns *navigations) pop() {

}

func (ns *navigations) popMultiple(amount int) {

}

func (ns navigations) resolve(b *board, p *piece) {
	for i := range ns {
		ns[i].resolve(b, p, i)
	}

}

func (n navigation) resolve(b *board, p *piece, index int) {

	if n.MoveLeft == true {
		p.moveLeft(b)
	}

	if n.WallKickLeft == true {
		p.wallKickLeft(b)
	}

	if n.WallKickRight == true {
		p.wallKickRight(b)
	}

	if n.MoveRight == true {
		p.moveRight(b)
	}

	if n.MoveDown == true {
		p.moveDown(b)
	}

	if n.Drop == true {
		p.drop(b)
	}

	if n.RotateLeft == true {
		p.rotatePiece(b, false)
	}

	if n.RotateRight == true {
		p.rotatePiece(b, true)
	}

	if n.Rotate180 == true {
		// not yet implemented
	}

	// would be nice to get this working in a nice way
	// navigation := navigation{}
	// navigation.movement.MoveLeft = true
	//
	// switch n {
	//
	// case navigation:
	// 	fmt.Println("MoveLeft")
	//
	// default:
	// 	fmt.Println(n)
	//
	// }

}

func (ns navigations) internalSmartResolveSoft() ([]string, []string, []string) {
	// var smartMoveList []string
	// var downList []string
	var preDownlist []string
	var postDownList []string
	var moveList []string

	for i := range ns {
		moveList = append(moveList, ns[i].mockresolve())
	}

	// first thing we want to do is isolate the string of sequential movedowns
	for i := 0; i < len(moveList); i++ {
		if moveList[i] == "moveDown" {
			preDownlist = moveList[:i]
			moveList = moveList[i:]
			break
		}
	}
	for i := 0; i < len(moveList); i++ {
		if moveList[i] != "moveDown" {
			postDownList = moveList[i:]
			moveList = moveList[:i]
			break
		}
	}

	// fmt.Println("predown", preDownlist)
	// fmt.Println("postdown", postDownList)
	// fmt.Println("downList", moveList)

	var finalmoveList []string
	finalmoveList = append(finalmoveList, moveList...)

	// we do not need this anymore as its handled on the py side of things as a keypress hold
	var extradowns int
	for i := 0; i < extradowns; i++ {
		finalmoveList = append(finalmoveList, "moveDown")
	}

	// finalmoveList = append(preDownlist, finalmoveList...)
	// finalmoveList = append(finalmoveList, postDownList...)
	// finalmoveList = append(finalmoveList, "drop")

	postDownList = append(postDownList, "drop")

	return preDownlist, finalmoveList, postDownList
}

func (ns navigations) smartresolveSoft() []string {
	// var smartMoveList []string
	// var downList []string
	var preDownlist []string
	var postDownList []string
	var moveList []string

	for i := range ns {
		moveList = append(moveList, ns[i].mockresolve())
	}

	// first thing we want to do is isolate the string of sequential movedowns
	for i := 0; i < len(moveList); i++ {
		if moveList[i] == "moveDown" {
			preDownlist = moveList[:i]
			moveList = moveList[i:]
			break
		}
	}
	for i := 0; i < len(moveList); i++ {
		if moveList[i] != "moveDown" {
			postDownList = moveList[i:]
			moveList = moveList[:i]
			break
		}
	}

	// fmt.Println("predown", preDownlist)
	// fmt.Println("postdown", postDownList)
	// fmt.Println("downList", moveList)

	var finalmoveList []string
	finalmoveList = append(finalmoveList, moveList...)

	// we do not need this anymore as its handled on the py side of things as a keypress hold
	var extradowns int
	for i := 0; i < extradowns; i++ {
		finalmoveList = append(finalmoveList, "moveDown")
	}

	finalmoveList = append(preDownlist, finalmoveList...)
	finalmoveList = append(finalmoveList, postDownList...)
	finalmoveList = append(finalmoveList, "drop")

	return finalmoveList
}

func (ns navigations) smartresolve() []string {
	// Does the same as mockresolve, but trims the moveList to minimize movement
	// NOTE: this only works for hard drop moves at the moment. Slides, spins etc. can not use this
	// TODO: above

	var moveList []string
	var prunedMoveList []string
	for i := range ns {
		moveList = append(moveList, ns[i].mockresolve())
	}

	for i := range moveList {
		if moveList[i] != "moveDown" {
			prunedMoveList = append(prunedMoveList, moveList[i])
		}
	}

	prunedMoveList = append(prunedMoveList, "drop")

	return prunedMoveList
}

func (ns navigations) mockresolve() []string {
	// Prints the instructions out rather than doing them
	var moveList []string
	for i := range ns {
		moveList = append(moveList, ns[i].mockresolve())
	}

	return moveList
}

func (n navigation) mockresolve() string {
	var move string

	if n.MoveLeft == true {
		// fmt.Println("MOVE LEFT")
		move = "moveLeft"
	}

	if n.WallKickLeft == true {
		// fmt.Println("WALL KICK LEFT")
		move = "kickLeft"
	}

	if n.WallKickRight == true {
		// fmt.Println("WALL KICK RIGHT")
		move = "kickRight"
	}

	if n.MoveRight == true {
		// fmt.Println("MOVE RIGHT")
		move = "moveRight"
	}

	if n.MoveDown == true {
		// fmt.Println("MOVE DOWN")
		move = "moveDown"
	}

	if n.Drop == true {
		// fmt.Println("DROP")
		move = "drop"
	}

	if n.RotateLeft == true {
		// fmt.Println("ROTATE LEFT")
		move = "rotateLeft"
	}

	if n.RotateRight == true {
		// fmt.Println("ROTATE RIGHT")
		move = "rotateRight"
	}

	if n.Rotate180 == true {
		// not yet implemented
	}
	return move
}
